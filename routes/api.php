<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1'], function () {
    Route::group(['prefix' => 'admins', 'middleware' => ['enforce_json']], function ($router) {
        Route::get('/','AdminController@index');
        Route::post('/','AdminController@store');
        Route::get('/show','AdminController@show');
        Route::put('/','AdminController@edit');
        Route::put('/{id}','AdminController@editFromSuperAdmin');
        Route::delete('/','AdminController@destroy');
        Route::delete('/{id}','AdminController@destroyFromSuperAdmin');
        Route::post('/login','AdminController@login');
        Route::post('/logout','AdminController@logout');
        Route::resource('suppliers','AdminSupplierController')->except('update');
        Route::post('suppliers/{id}', 'AdminSupplierController@update');
        Route::resource('users','AdminUserController');
        Route::resource('supplier_packages','SupplierPackageController');
    });


    Route::group(['prefix' => 'suppliers', 'middleware' => ['enforce_json']], function ($router) {

        Route::get('/','SupplierController@index');
        Route::post('/','SupplierController@register');
        Route::get('/show','SupplierController@show');
        Route::post('/update','SupplierController@edit');
        Route::delete('/','SupplierController@destroy');
        Route::delete('/{attachmentId}','SupplierController@removeAttachment');

        Route::resource('messages','SupplierMessageController');

            Route::get('/verified-only', function(Request $request){

                dd('your are verified', $request->user()->name);
            })->middleware('auth:suppliers','verified');

        Route::post('/login','SupplierController@login');
        Route::post('/logout','SupplierController@logout');
        Route::get('/email/send_verfiation', 'SupplierController@resend')->name('verification.resend');
    });

    Route::group(['prefix' => 'users', 'middleware' => ['enforce_json']], function ($router) {
        Route::get('/','UserController@index');
        Route::post('/','UserController@register');
        Route::get('/show','UserController@show');
        Route::put('/','UserController@edit');
        Route::delete('/','UserController@destroy');


        Route::get('/favorite','UserController@getFavorites');
        Route::post('/favorite/{productId}','UserController@setFavorite');

        Route::resource('messages','UserMessageController');

        Route::get('/verified-only', function(Request $request){

            dd('your are verified', $request->user()->name);
        })->middleware('auth:users','verified');

        Route::post('/login','UserController@login');
        Route::post('/logout','UserController@logout');
        Route::get('/email/send_verfiation', 'UserController@resend')->name('verification.resend');
    });
    Route::get('/email/verify/{id}/{hash}', 'VerificationController@verify')->name('verification.verify');

    Route::group(['prefix' => 'products', 'middleware' => ['enforce_json']], function ($router) {
        Route::get('/','ProductController@index');
        Route::get('/expiredProducts','ProductController@expiredProducts');
        Route::get('/supplier','ProductController@getBySupplierId');
        Route::get('/search','ProductController@search');
        Route::get('/getByPrice','ProductController@getByPrice');
        Route::get('/category/{categoryId}','ProductController@getByCategoryId');
        Route::get('enddate/{enddate}','ProductController@getByEndDate');
        Route::get('/sub_category/{subCategoryId}','ProductController@getBySubCategoryId');
        Route::get('/lat_lng','ProductController@getByLatLng');
        Route::post('/','ProductController@store');
        Route::get('/{id}','ProductController@show');
        Route::post('/{id}','ProductController@edit');
        Route::delete('/{id}','ProductController@destroy');
        Route::delete('/image/{id}','ProductController@removeImage');
    });

    Route::group(['prefix' => 'categories', 'middleware' => ['enforce_json']], function ($router) {
        Route::get('/','CategoryController@index');
        Route::post('/','CategoryController@store');
        Route::get('/{id}','CategoryController@show');
        Route::post('/{id}','CategoryController@update');
        Route::delete('/{id}','CategoryController@destroy');
    });

    Route::group(['prefix' => 'sub_categories', 'middleware' => ['enforce_json']], function ($router) {
        Route::get('/','SubCategoryController@index');
        Route::get('/category/{categoryId}','SubCategoryController@getByCategoryId');
        Route::post('/','SubCategoryController@store');
        Route::get('/{id}','SubCategoryController@show');
        Route::post('/{id}','SubCategoryController@update');
        Route::delete('/{id}','SubCategoryController@destroy');
    });

    Route::group(['prefix' => 'countries', 'middleware' => ['enforce_json']], function ($router) {
        Route::get('/','CountryController@index');
        Route::post('/','CountryController@store');
        Route::get('/{id}','CountryController@show');
        Route::post('/{id}','CountryController@update');
        Route::delete('/{id}','CountryController@destroy');
    });

    Route::group(['prefix' => 'cities', 'middleware' => ['enforce_json']], function ($router) {
        Route::get('/','CityController@index');
        Route::get('/country/{countryId}','CityController@getByCountryId');
        Route::post('/','CityController@store');
        Route::get('/{id}','CityController@show');
        Route::post('/{id}','CityController@update');
        Route::delete('/{id}','CityController@destroy');
    });

    Route::group(['prefix' => 'stores', 'middleware' => ['enforce_json']], function ($router) {
        Route::get('/','StoreController@index');
        Route::get('/supplier/','StoreController@getBySupplierId');
        Route::post('/','StoreController@store');
        Route::get('/{id}','StoreController@show');
        Route::post('/{id}','StoreController@update');
        Route::delete('/{id}','StoreController@destroy');
    });

    Route::group(['prefix' => 'sub_stores', 'middleware' => ['enforce_json']], function ($router) {
        Route::get('/','SubStoreController@index');
        Route::get('/store/{storeId}','SubStoreController@getByStoreId');
        Route::post('/','SubStoreController@store');
        Route::get('/{id}','SubStoreController@show');
        Route::post('/{id}','SubStoreController@update');
        Route::delete('/{id}','SubStoreController@destroy');
    });

});



