-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 05, 2020 at 01:49 AM
-- Server version: 10.4.8-MariaDB
-- PHP Version: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `supplier`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `plain_password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `role` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `phone`, `password`, `plain_password`, `username`, `email`, `remember_token`, `role`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'super_admin', '123456789', '$2y$10$6AR8SpuG3xsVVnYNAS.ZCeksEIm3RMmfJtf/yATsPbqAqCfBo43Uq', '123456', 'superadmin', 'super@admin.com', NULL, 2, NULL, '2020-04-30 16:45:45', '2020-05-04 14:21:39'),
(4, 'mohammed', '0797071446', '$2y$10$HRqymFwIa/Kk9YzavxmK/.3mTdhr3ONcy.RIpjov5D1nBupqKfrwe', '1234567', 'mohammedd', 'mohammedd@admin.com', NULL, 1, NULL, '2020-05-04 14:15:07', '2020-05-04 14:22:34');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `ar_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `icon_photo` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `ar_name`, `en_name`, `icon_photo`, `created_at`, `updated_at`) VALUES
(5, 'الفئة 2', 'category 2', '202005041910exTH.jpeg', '2020-05-04 16:47:10', '2020-05-04 16:47:10');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `ar_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `ar_photo` text NOT NULL,
  `en_photo` text NOT NULL,
  `country_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `ar_name`, `en_name`, `ar_photo`, `en_photo`, `country_id`, `created_at`, `updated_at`) VALUES
(4, 'عمانن', 'Amman', '202005041939JomX.jpeg', '202005041939atLC.jpeg', 3, '2020-05-04 16:55:25', '2020-05-04 16:56:39');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `id` int(11) NOT NULL,
  `ar_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `ar_photo` text NOT NULL,
  `en_photo` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `ar_name`, `en_name`, `ar_photo`, `en_photo`, `created_at`, `updated_at`) VALUES
(3, 'الاردن', 'jordan', '202005041907MyL3.png', '202005041907PfWI.jpeg', '2020-05-04 16:53:07', '2020-05-04 16:53:07'),
(4, 'الكويتت', 'Kuwait', '202005041925yPbW.png', '202005041925bXPC.jpeg', '2020-05-04 16:53:45', '2020-05-04 16:54:25');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `body` text NOT NULL,
  `sender` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `supplier_id`, `user_id`, `body`, `sender`, `created_at`, `updated_at`) VALUES
(11, 9, 11, 'Hi Supplier', 'user', '2020-05-04 20:45:16', '2020-05-04 20:45:16'),
(12, 9, 11, 'Hi User', 'supplier', '2020-05-04 20:46:35', '2020-05-04 20:46:35');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `ar_title` varchar(255) NOT NULL,
  `en_title` varchar(255) NOT NULL,
  `ar_description` text NOT NULL,
  `en_description` text NOT NULL,
  `sub_store_ids` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `old_price` float NOT NULL,
  `sale_price` float NOT NULL,
  `end_date` datetime NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `ar_title`, `en_title`, `ar_description`, `en_description`, `sub_store_ids`, `category_id`, `old_price`, `sale_price`, `end_date`, `supplier_id`, `created_at`, `updated_at`) VALUES
(6, 'منتجج 2', 'product 2', 'شرح للمنتج رقم 2', 'description for product 2', '3', 5, 11, 5, '2020-05-02 00:00:00', 9, '2020-05-04 17:24:59', '2020-05-04 17:25:39');

-- --------------------------------------------------------

--
-- Table structure for table `product_photos`
--

CREATE TABLE `product_photos` (
  `id` int(11) NOT NULL,
  `photo_name` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_photos`
--

INSERT INTO `product_photos` (`id`, `photo_name`, `product_id`, `created_at`, `updated_at`) VALUES
(11, '2020050420597iHl.jpeg', 6, '2020-05-04 17:24:59', '2020-05-04 17:24:59'),
(12, '202005042059wXjU.jpeg', 6, '2020-05-04 17:24:59', '2020-05-04 17:24:59');

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE `stores` (
  `id` int(11) NOT NULL,
  `ar_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `ar_name`, `en_name`, `lat`, `lng`, `supplier_id`, `created_at`, `updated_at`) VALUES
(6, 'متجر محمد', 'mohamed store', 31.54561, 35.65454, 9, '2020-05-04 17:15:37', '2020-05-04 17:15:37'),
(7, 'متجر خالد الدبوبي', 'Khalid Aldaboubi Store', 30.65451, 34.8561, 9, '2020-05-04 17:15:58', '2020-05-04 17:16:52');

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(11) NOT NULL,
  `ar_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `icon_photo` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `ar_name`, `en_name`, `icon_photo`, `category_id`, `created_at`, `updated_at`) VALUES
(3, 'تصنيف فرعيي 1', 'sub category 1', '202005041933w16L.png', 5, '2020-05-04 16:49:40', '2020-05-04 16:51:33'),
(4, 'تصنيف فرعي 2', 'sub scategory 2', '2020050419019mac.jpeg', 5, '2020-05-04 16:50:01', '2020-05-04 16:50:01'),
(5, 'تصنيف فرعي 3', 'sub scategory 3', '202005041906AwMw.jpeg', 5, '2020-05-04 16:50:06', '2020-05-04 16:50:06'),
(6, 'تصنيف فرعي 4', 'sub scategory 4', '202005041912DPTJ.jpeg', 5, '2020-05-04 16:50:12', '2020-05-04 16:50:12');

-- --------------------------------------------------------

--
-- Table structure for table `sub_stores`
--

CREATE TABLE `sub_stores` (
  `id` int(11) NOT NULL,
  `ar_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `store_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sub_stores`
--

INSERT INTO `sub_stores` (`id`, `ar_name`, `en_name`, `lat`, `lng`, `store_id`, `created_at`, `updated_at`) VALUES
(3, 'فرع عمان', 'Amman branch', 30.65451, 34.8561, 7, '2020-05-04 17:21:49', '2020-05-04 17:22:37');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `registration_number` varchar(255) NOT NULL,
  `land_line` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `plain_password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) NOT NULL,
  `rate` int(10) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `company_name`, `registration_number`, `land_line`, `location`, `category_id`, `phone`, `password`, `plain_password`, `username`, `email`, `remember_token`, `full_name`, `rate`, `email_verified_at`, `created_at`, `updated_at`) VALUES
(7, 'mohammed new company', '124651652', '45662', 'amman', 1, '0797071441', '$2y$12$JyZboEoBZ7nGopFswgbx2.ftCPP3nMTW9MRxSGL3dSx/R5C56CFgG', '1234567', 'mohammed', 'mohammed@alhisba.com', NULL, 'khalid aldaboubi', 4, '2020-05-04 16:04:28', '2020-05-04 16:04:28', '2020-05-04 16:09:14'),
(9, 'mohammed  aldaboubi company', '1246516512', '45662', 'amman', 1, '0797071440', '$2y$10$b16w9Imq.oYseeD9gQs.3.bcd5OclILe2z/5gD6rX4qtTUB6zBqt2', '1234567', 'aldaboubi1', 'k.hussein@alhisba.com', NULL, 'mohammed', 4, '2020-05-04 17:01:38', '2020-05-04 16:59:05', '2020-05-04 17:12:11');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_attachments`
--

CREATE TABLE `supplier_attachments` (
  `id` int(11) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplier_attachments`
--

INSERT INTO `supplier_attachments` (`id`, `file_name`, `supplier_id`, `created_at`, `updated_at`) VALUES
(8, '202005041928w6eO.xlsx', 7, '2020-05-04 16:04:28', '2020-05-04 16:04:28'),
(9, '202005041928klBk.txt', 7, '2020-05-04 16:04:28', '2020-05-04 16:04:28'),
(12, '2020050419054DCI.txt', 9, '2020-05-04 16:59:07', '2020-05-04 16:59:07'),
(13, '202005041907SZnP.xlsx', 9, '2020-05-04 16:59:07', '2020-05-04 16:59:07');

-- --------------------------------------------------------

--
-- Table structure for table `supplier_packages`
--

CREATE TABLE `supplier_packages` (
  `id` int(11) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `allow_post` int(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `supplier_packages`
--

INSERT INTO `supplier_packages` (`id`, `supplier_id`, `allow_post`, `created_at`, `updated_at`) VALUES
(2, 9, 20, '2020-05-04 17:41:44', '2020-05-04 17:41:44');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `plain_password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `address` text DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `password`, `plain_password`, `username`, `email`, `remember_token`, `address`, `country_id`, `city_id`, `street`, `email_verified_at`, `created_at`, `updated_at`) VALUES
(11, 'khalid aldaboubi', '0797071441', '$2y$10$OH0c34s86Sa92QqybUuMH.hcKaqz85EO9ALLYuozQ2jm3qWihDFPO', '1234567', 'khalid', 'khalid@gmail.com', NULL, 'sahab', 2, 1, 'sahat st', '2020-05-04 16:37:25', '2020-05-04 16:34:28', '2020-05-04 16:37:25');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`phone`),
  ADD UNIQUE KEY `password` (`password`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_photos`
--
ALTER TABLE `product_photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stores`
--
ALTER TABLE `stores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_stores`
--
ALTER TABLE `sub_stores`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`phone`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `supplier_attachments`
--
ALTER TABLE `supplier_attachments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplier_packages`
--
ALTER TABLE `supplier_packages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `phone` (`phone`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `product_photos`
--
ALTER TABLE `product_photos`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `stores`
--
ALTER TABLE `stores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sub_stores`
--
ALTER TABLE `sub_stores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `supplier_attachments`
--
ALTER TABLE `supplier_attachments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `supplier_packages`
--
ALTER TABLE `supplier_packages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
