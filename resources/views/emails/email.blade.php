@component('mail::message')
{{-- Greeting --}}

@isset($notifiable->company_name)
    Hello {{ $notifiable->company_name }}
    @else
    Hello {{ $notifiable->name }}
@endisset


{{-- Intro Lines --}}
@foreach ($introLines as $line)
{{ $line }}

@endforeach

{{-- Action Button --}}

<?php
    switch ($level) {
        case 'success':
        case 'error':
            $color = $level;
            break;
        default:
            $color = 'primary';
    }
?>
@component('mail::button', ['url' => $url, 'color' => $color])
Clicking To Verify Your Email
@endcomponent


{{-- Outro Lines --}}
@foreach ($outroLines as $line)
{{ $line }}

@endforeach

{{-- Salutation --}}
@if (! empty($salutation))
{{ $salutation }}
@else
@lang('Regards'),<br>
{{ config('app.name') }}
@endif

{{-- Subcopy --}}
@slot('subcopy')
<span class="break-all">If you’re having trouble clicking the button, copy and paste the URL below <br>({{ $url }})</span>
@endslot
@endcomponent
