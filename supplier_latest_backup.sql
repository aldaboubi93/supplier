-- MySQL dump 10.13  Distrib 8.0.18, for Win64 (x86_64)
--
-- Host: localhost    Database: supplier
-- ------------------------------------------------------
-- Server version	5.7.30-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admins`
--

DROP TABLE IF EXISTS `admins`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admins` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `plain_password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `role` int(11) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phone` (`phone`),
  UNIQUE KEY `password` (`password`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admins`
--

LOCK TABLES `admins` WRITE;
/*!40000 ALTER TABLE `admins` DISABLE KEYS */;
INSERT INTO `admins` VALUES (1,'super_admin','123456789','$2y$10$6AR8SpuG3xsVVnYNAS.ZCeksEIm3RMmfJtf/yATsPbqAqCfBo43Uq','123456','superadmin','super@admin.com',NULL,2,NULL,'2020-04-30 16:45:45','2020-05-04 14:21:39'),(4,'mohammed','0797071446','$2y$10$HRqymFwIa/Kk9YzavxmK/.3mTdhr3ONcy.RIpjov5D1nBupqKfrwe','1234567','mohammedd','mohammedd@admin.com',NULL,1,NULL,'2020-05-04 14:15:07','2020-05-04 14:22:34'),(5,'mohammed','0797071441','$2y$10$3uw1SnJ9TsCWgg66unbajuFhg7ilu0mzYQTPxB.9p.gdJl7edIyRm','1234567','mohammed','mohammed@admin.com',NULL,1,NULL,'2020-05-05 17:29:15','2020-05-05 17:29:15');
/*!40000 ALTER TABLE `admins` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ar_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `icon_photo` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (5,'الفئة 2','category 2','202005041910exTH.jpeg','2020-05-04 16:47:10','2020-05-04 16:47:10'),(6,'Medical','Medical','202005051701ir0w.jpeg','2020-05-05 17:58:01','2020-05-05 17:58:01'),(7,'Food','Food','202005051726m4ko.jpeg','2020-05-05 17:58:26','2020-05-05 17:58:26'),(8,'Pharmacy Products','Pharmacy Products','202005051734nAL5.jpeg','2020-05-05 17:58:34','2020-05-05 17:58:34'),(9,'Device Products','Device Products','202005051746eFCr.jpeg','2020-05-05 17:58:46','2020-05-05 17:58:46'),(10,'Chemical Products','Chemical Products','202005051756caBT.jpeg','2020-05-05 17:58:56','2020-05-05 17:58:56'),(11,'Cosmetics Products','Cosmetics Products','202005051704ypWP.jpeg','2020-05-05 17:59:04','2020-05-05 17:59:04'),(12,'Others','Others','2020050517138dnm.jpeg','2020-05-05 17:59:13','2020-05-05 17:59:13');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cities`
--

DROP TABLE IF EXISTS `cities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ar_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `ar_photo` text NOT NULL,
  `en_photo` text NOT NULL,
  `country_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cities`
--

LOCK TABLES `cities` WRITE;
/*!40000 ALTER TABLE `cities` DISABLE KEYS */;
INSERT INTO `cities` VALUES (4,'عمانن','Amman','202005041939JomX.jpeg','202005041939atLC.jpeg',3,'2020-05-04 16:55:25','2020-05-04 16:56:39');
/*!40000 ALTER TABLE `cities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `countries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ar_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `ar_photo` text NOT NULL,
  `en_photo` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (3,'الاردن','jordan','202005041907MyL3.png','202005041907PfWI.jpeg','2020-05-04 16:53:07','2020-05-04 16:53:07'),(4,'الكويتت','Kuwait','202005041925yPbW.png','202005041925bXPC.jpeg','2020-05-04 16:53:45','2020-05-04 16:54:25'),(5,'قطر','qatar','202005051525ppzy.jpeg','20200505152537GC.png','2020-05-05 15:02:25','2020-05-05 15:02:25');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorites`
--

DROP TABLE IF EXISTS `favorites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `product_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorites`
--

LOCK TABLES `favorites` WRITE;
/*!40000 ALTER TABLE `favorites` DISABLE KEYS */;
/*!40000 ALTER TABLE `favorites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `body` text NOT NULL,
  `sender` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `messages`
--

LOCK TABLES `messages` WRITE;
/*!40000 ALTER TABLE `messages` DISABLE KEYS */;
INSERT INTO `messages` VALUES (11,9,11,'Hi Supplier','user','2020-05-04 20:45:16','2020-05-04 20:45:16'),(12,9,11,'Hi User','supplier','2020-05-04 20:46:35','2020-05-04 20:46:35');
/*!40000 ALTER TABLE `messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `product_photos`
--

DROP TABLE IF EXISTS `product_photos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `product_photos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `photo_name` varchar(255) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `product_photos`
--

LOCK TABLES `product_photos` WRITE;
/*!40000 ALTER TABLE `product_photos` DISABLE KEYS */;
INSERT INTO `product_photos` VALUES (11,'2020050420597iHl.jpeg',6,'2020-05-04 17:24:59','2020-05-04 17:24:59'),(12,'202005042059wXjU.jpeg',6,'2020-05-04 17:24:59','2020-05-04 17:24:59'),(14,'202005081805AB9l.png',11,'2020-05-08 18:43:05','2020-05-08 18:43:05'),(15,'202005081905UiIE.png',21,'2020-05-08 19:52:05','2020-05-08 19:52:05'),(16,'202005081912N6bM.png',22,'2020-05-08 19:52:12','2020-05-08 19:52:12'),(17,'202005081920rE0I.png',23,'2020-05-08 19:52:20','2020-05-08 19:52:20'),(18,'202005082025Syuq.png',25,'2020-05-08 20:01:25','2020-05-08 20:01:25'),(19,'202005082029p2ZN.png',28,'2020-05-08 20:02:29','2020-05-08 20:02:29'),(20,'202005082015Y2WN.png',29,'2020-05-08 20:05:15','2020-05-08 20:05:15'),(21,'202005082016BO19.png',30,'2020-05-08 20:05:16','2020-05-08 20:05:16');
/*!40000 ALTER TABLE `product_photos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `products` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ar_title` varchar(255) NOT NULL,
  `en_title` varchar(255) NOT NULL,
  `ar_description` text NOT NULL,
  `en_description` text NOT NULL,
  `sub_store_ids` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `old_price` float NOT NULL,
  `sale_price` float NOT NULL,
  `end_date` datetime NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `phone` varchar(255) DEFAULT NULL,
  `location` text,
  `sub_category_id` int(45) DEFAULT NULL,
  `barcode` varchar(255) DEFAULT NULL,
  `qty` int(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` VALUES (6,'منتجج 2','product 2','شرح للمنتج رقم 2','description for product 2','3',5,11,5,'2020-05-02 00:00:00',9,'2020-05-04 17:24:59','2020-05-04 17:25:39',NULL,NULL,NULL,NULL,NULL),(7,'منتج 1','product 1','شرح للمنتج رقم 1','description for product 1','3',5,10,5,'2020-05-02 00:00:00',10,'2020-05-08 15:09:38','2020-05-08 15:09:38',NULL,NULL,NULL,NULL,NULL),(9,'w','w','www','www','6',5,0,0,'2222-02-22 00:00:00',18,'2020-05-08 18:26:39','2020-05-08 18:26:39','w','w',6,'w',NULL),(10,'w','w','www','www','6',5,0,0,'2222-02-22 00:00:00',18,'2020-05-08 18:30:31','2020-05-08 18:30:31','079','w',6,'w',2),(11,'منتج 1','product 1','شرح للمنتج رقم 1','description for product 1','3',5,10,5,'2020-05-02 00:00:00',18,'2020-05-08 18:43:05','2020-05-08 18:43:05','0','amman',6,'22222',10),(12,'p3','p3','ww','ww','6',11,0,0,'2222-02-22 00:00:00',19,'2020-05-08 19:20:51','2020-05-08 19:20:51','2222','ww',6,'p3',21),(21,'dsfsdf','w','www','www','6',5,0,0,'2222-02-22 00:00:00',19,'2020-05-08 19:52:05','2020-05-08 19:52:05','w','w',6,'w',2),(22,'dsfsdf','w','www','www','6',5,0,0,'2222-02-22 00:00:00',19,'2020-05-08 19:52:12','2020-05-08 19:52:12','w','w',6,'w',2),(23,'dsfsdf','w','www','www','6',5,0,0,'2222-02-22 00:00:00',19,'2020-05-08 19:52:20','2020-05-08 19:52:20','w','w',6,'w',2),(24,'dsfsdf','w','www','www','6',5,0,0,'2222-02-22 00:00:00',19,'2020-05-08 20:01:24','2020-05-08 20:01:24','w','w',6,'w',2),(25,'منتج 1','product 1','شرح للمنتج رقم 1','description for product 1','3',5,10,5,'2020-05-02 00:00:00',19,'2020-05-08 20:01:25','2020-05-08 20:01:25','0','amman',6,'22222',10),(26,'dsfsdf','w','www','www','6',5,0,0,'2222-02-22 00:00:00',19,'2020-05-08 20:02:17','2020-05-08 20:02:17','w','w',6,'w',2),(27,'dsfsdf','w','www','www','6',5,0,0,'2222-02-22 00:00:00',19,'2020-05-08 20:02:24','2020-05-08 20:02:24','w','w',6,'w',2),(28,'dsfsdf','w','www','www','6',5,0,0,'2222-02-22 00:00:00',19,'2020-05-08 20:02:29','2020-05-08 20:02:29','w','w',6,'w',2),(29,'منتج 1','product 1','شرح للمنتج رقم 1','description for product 1','3',5,10,5,'2020-05-02 00:00:00',19,'2020-05-08 20:05:15','2020-05-08 20:05:15','0','amman',6,'22222',10),(30,'dsfsdf','w','www','www','6',5,0,0,'2222-02-22 00:00:00',19,'2020-05-08 20:05:16','2020-05-08 20:05:16','w','w',6,'w',2);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stores`
--

DROP TABLE IF EXISTS `stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ar_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stores`
--

LOCK TABLES `stores` WRITE;
/*!40000 ALTER TABLE `stores` DISABLE KEYS */;
INSERT INTO `stores` VALUES (6,'متجر محمد','mohamed store',31.54561,35.65454,9,'2020-05-04 17:15:37','2020-05-04 17:15:37'),(7,'متجر خالد الدبوبي','Khalid Aldaboubi Store',30.65451,34.8561,9,'2020-05-04 17:15:58','2020-05-04 17:16:52'),(8,'متجر خالد','khalid store',31.54561,35.65454,10,'2020-05-08 13:16:41','2020-05-08 13:16:41'),(9,'سامح مول','سامح مول',0,0,10,'2020-05-08 16:23:28','2020-05-08 16:23:28'),(10,'سامح مول','سامح مول',0,0,18,'2020-05-08 17:47:44','2020-05-08 17:47:44');
/*!40000 ALTER TABLE `stores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_categories`
--

DROP TABLE IF EXISTS `sub_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sub_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ar_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `icon_photo` text NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_categories`
--

LOCK TABLES `sub_categories` WRITE;
/*!40000 ALTER TABLE `sub_categories` DISABLE KEYS */;
INSERT INTO `sub_categories` VALUES (3,'تصنيف فرعيي 1','sub category 1','202005041933w16L.png',5,'2020-05-04 16:49:40','2020-05-04 16:51:33'),(4,'تصنيف فرعي 2','sub scategory 2','2020050419019mac.jpeg',5,'2020-05-04 16:50:01','2020-05-04 16:50:01'),(5,'تصنيف فرعي 3','sub scategory 3','202005041906AwMw.jpeg',5,'2020-05-04 16:50:06','2020-05-04 16:50:06'),(6,'تصنيف فرعي 4','sub scategory 4','202005041912DPTJ.jpeg',5,'2020-05-04 16:50:12','2020-05-04 16:50:12');
/*!40000 ALTER TABLE `sub_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sub_stores`
--

DROP TABLE IF EXISTS `sub_stores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sub_stores` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ar_name` varchar(255) NOT NULL,
  `en_name` varchar(255) NOT NULL,
  `lat` double NOT NULL,
  `lng` double NOT NULL,
  `store_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sub_stores`
--

LOCK TABLES `sub_stores` WRITE;
/*!40000 ALTER TABLE `sub_stores` DISABLE KEYS */;
INSERT INTO `sub_stores` VALUES (3,'فرع عمان','Amman branch',30.65451,34.8561,7,'2020-05-04 17:21:49','2020-05-04 17:22:37'),(5,'فرع عمان','فرع عمان',31.54561,31.54561,8,'2020-05-08 17:09:37','2020-05-08 17:09:37'),(6,'فرع عمان','فرع عمان',0,0,10,'2020-05-08 17:48:05','2020-05-08 17:48:05');
/*!40000 ALTER TABLE `sub_stores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier_attachments`
--

DROP TABLE IF EXISTS `supplier_attachments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `supplier_attachments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(255) NOT NULL,
  `supplier_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_attachments`
--

LOCK TABLES `supplier_attachments` WRITE;
/*!40000 ALTER TABLE `supplier_attachments` DISABLE KEYS */;
INSERT INTO `supplier_attachments` VALUES (8,'202005041928w6eO.xlsx',7,'2020-05-04 16:04:28','2020-05-04 16:04:28'),(9,'202005041928klBk.txt',7,'2020-05-04 16:04:28','2020-05-04 16:04:28'),(12,'2020050419054DCI.txt',9,'2020-05-04 16:59:07','2020-05-04 16:59:07'),(13,'202005041907SZnP.xlsx',9,'2020-05-04 16:59:07','2020-05-04 16:59:07'),(14,'202005051414AIMb.txt',10,'2020-05-05 14:44:14','2020-05-05 14:44:14'),(15,'202005051414BDhQ.xlsx',10,'2020-05-05 14:44:14','2020-05-05 14:44:14'),(16,'202005051521AlI6.txt',10,'2020-05-05 15:11:21','2020-05-05 15:11:21'),(17,'202005051521htpm.xlsx',10,'2020-05-05 15:11:21','2020-05-05 15:11:21'),(18,'202005051546aFjg.txt',10,'2020-05-05 15:14:46','2020-05-05 15:14:46'),(19,'202005051546tUSO.xlsx',10,'2020-05-05 15:14:46','2020-05-05 15:14:46'),(20,'202005051553sweY.txt',10,'2020-05-05 15:14:53','2020-05-05 15:14:53'),(21,'202005051553CTwd.xlsx',10,'2020-05-05 15:14:53','2020-05-05 15:14:53'),(22,'202005051517uNOi.txt',10,'2020-05-05 15:16:17','2020-05-05 15:16:17'),(23,'202005051517z5Iz.xlsx',10,'2020-05-05 15:16:17','2020-05-05 15:16:17'),(24,'202005051551XPpH.txt',10,'2020-05-05 15:16:51','2020-05-05 15:16:51'),(25,'202005051551qnof.xlsx',10,'2020-05-05 15:16:51','2020-05-05 15:16:51'),(26,'202005051512ULFe.txt',10,'2020-05-05 15:17:12','2020-05-05 15:17:12'),(27,'202005051512b4CJ.xlsx',10,'2020-05-05 15:17:12','2020-05-05 15:17:12'),(28,'202005051634vXvO.txt',10,'2020-05-05 16:13:34','2020-05-05 16:13:34'),(29,'202005051634dVSA.xlsx',10,'2020-05-05 16:13:34','2020-05-05 16:13:34'),(30,'202005051653WJHK.txt',11,'2020-05-05 16:14:53','2020-05-05 16:14:53'),(31,'202005051653nx04.xlsx',11,'2020-05-05 16:14:53','2020-05-05 16:14:53'),(32,'202005051721irsf.txt',12,'2020-05-05 17:34:21','2020-05-05 17:34:21'),(33,'202005081757Xsnm.txt',17,'2020-05-08 17:23:57','2020-05-08 17:23:57'),(34,'202005081757JxRh.txt',17,'2020-05-08 17:23:57','2020-05-08 17:23:57'),(35,'2020050817500NBf.txt',18,'2020-05-08 17:25:50','2020-05-08 17:25:50'),(36,'202005081750Aqo6.txt',18,'2020-05-08 17:25:50','2020-05-08 17:25:50'),(37,'202005081737Vzkj.xlsx',18,'2020-05-08 17:26:37','2020-05-08 17:26:37'),(38,'202005081924sckk.txt',19,'2020-05-08 19:00:24','2020-05-08 19:00:24'),(39,'202005081924ELkN.xlsx',19,'2020-05-08 19:00:24','2020-05-08 19:00:24'),(40,'202005081954eLuA.jpeg',19,'2020-05-08 19:01:54','2020-05-08 19:01:54'),(41,'202005081954u1I6.png',19,'2020-05-08 19:01:54','2020-05-08 19:01:54'),(42,'202005081948uLwU.txt',20,'2020-05-08 19:02:48','2020-05-08 19:02:48'),(43,'202005081948Zbt0.txt',20,'2020-05-08 19:02:48','2020-05-08 19:02:48');
/*!40000 ALTER TABLE `supplier_attachments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `supplier_packages`
--

DROP TABLE IF EXISTS `supplier_packages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `supplier_packages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `supplier_id` int(11) NOT NULL,
  `allow_post` int(2) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `supplier_packages`
--

LOCK TABLES `supplier_packages` WRITE;
/*!40000 ALTER TABLE `supplier_packages` DISABLE KEYS */;
INSERT INTO `supplier_packages` VALUES (2,9,20,'2020-05-04 17:41:44','2020-05-04 17:41:44');
/*!40000 ALTER TABLE `supplier_packages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `suppliers`
--

DROP TABLE IF EXISTS `suppliers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `suppliers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `company_name` varchar(255) NOT NULL,
  `registration_number` varchar(255) NOT NULL,
  `land_line` varchar(255) NOT NULL,
  `location` varchar(255) NOT NULL,
  `category_id` int(11) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `plain_password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `full_name` varchar(255) NOT NULL,
  `rate` int(10) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phone` (`phone`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `suppliers`
--

LOCK TABLES `suppliers` WRITE;
/*!40000 ALTER TABLE `suppliers` DISABLE KEYS */;
INSERT INTO `suppliers` VALUES (7,'mohammed new company','124651652','45662','amman',1,'0797071441','$2y$12$JyZboEoBZ7nGopFswgbx2.ftCPP3nMTW9MRxSGL3dSx/R5C56CFgG','1234567','mohammed','mohammed@alhisba.com',NULL,'khalid aldaboubi',4,'2020-05-04 16:04:28','2020-05-04 16:04:28','2020-05-04 16:09:14'),(9,'mohammed  aldaboubi company','1246516512','45662','amman',1,'0797071440','$2y$10$b16w9Imq.oYseeD9gQs.3.bcd5OclILe2z/5gD6rX4qtTUB6zBqt2','1234567','aldaboubi1','k.hussein@alhisba.com',NULL,'mohammed',4,'2020-05-04 17:01:38','2020-05-04 16:59:05','2020-05-04 17:12:11'),(10,'mohammed company','1246516514','45662','amman',1,'0797071447','$2y$10$MFnloYcQHCrHgg9J1bWTGOKamnJXS6OrNSjH5XdQQITBlaMOSdCym','1234567','aldaboubi2','khalid.tutorial@gmail.com',NULL,'mohammed',4,'2020-05-05 16:05:47','2020-05-05 14:44:14','2020-05-05 16:05:47'),(11,'mohammed company','1246516517','45662','amman',1,'0797071449','$2y$10$X0mnb6CRSvnbLU47icQWUeNtuB0btbIpwd7PtwNeUDJj1ECxQ1YVW','1234567','aldaboubi4','khalid.tutorial2@gmail.com',NULL,'mohammed',4,NULL,'2020-05-05 16:14:53','2020-05-05 16:14:53'),(12,'khalid company','641321','45662','amman',1,'0797071442','$2y$10$sFFqXAFt0qOcmitRyAultOEhLzOfmks6GcOnOYogW97t.qChODfN6','1234567','khalid','khalid@alhisba.com',NULL,'khalid aldaboubi',4,'2020-05-05 17:34:21','2020-05-05 17:34:21','2020-05-05 17:34:21'),(13,'hmead','123123','079','Amman',11,'079','$2y$10$KjPYQgMYoA93y7CiW2wr4e0osV7QK0srDEeE8CwTiygFhXZIMAV1W','123123','hmead10','hmead@gmail.com',NULL,'mohammad hmead',NULL,NULL,'2020-05-08 12:16:34','2020-05-08 12:16:34'),(14,'hmead','1234567800','0791','Amman',11,'0792','$2y$10$u7aBBU8w.QQzvxC07taH3eYyHZhVx25akMX2K8Wia2wEUI8YuvXZC','123123','hmead20','hmead1@gmail.com',NULL,'mohammad hmead',NULL,NULL,'2020-05-08 12:17:24','2020-05-08 12:17:24'),(15,'hmead','akhfakjshjkas','079999992929292','Amman',11,'079999992929292','$2y$10$oUAzxT7U5e.7PTgo2RBLD.UGeGsx3odsIM2tvnqIWMnCT4qP6Oe5q','123123','hmead2020','hmeadooo@gmail.com',NULL,'mohammad hmead',NULL,NULL,'2020-05-08 12:19:00','2020-05-08 12:19:00'),(16,'m hmead','123009','0778','Amman',11,'07890','$2y$10$V.qoI/nzz0i6iPqmrXSdmeSgaT.AZROYvFoQ7Rq1Uz8eHMJQhpQ9y','123123','demo','demo1@gmail.com',NULL,'test demo',NULL,NULL,'2020-05-08 12:20:51','2020-05-08 12:20:51'),(17,'mohammed company','64165123','45662','amman',1,'0797071456','$2y$10$7C9JgKnyQm6MpRHvz2aEheKorXABt6TjWUWI6ACEPjV.KG6G6YRGa','1234567','khalid2','talal.daboubi97@gmail.com',NULL,'mohammed',4,NULL,'2020-05-08 17:23:53','2020-05-08 17:23:53'),(18,'mohammed company','641651234','45662','amman',1,'07970714564','$2y$10$7Ca4RoKfo7o/Ji3Kfe53U.X7lzwqn.zXr.uANH5t6oz93r3tpATlu','1234567','khalid4','khalid.aldaboubi93@gmail.com',NULL,'mohammed',4,'2020-05-08 17:30:49','2020-05-08 17:25:48','2020-05-08 17:30:49'),(19,'hmdeco','10020','079999','amman',11,'079866','$2y$10$SX5kH3Rd85IyQv0/9g8BMeKbPDfHWiHLBdpxzYfIwzkFX3e7pNij2','123123','code','codingforlive@gmail.com',NULL,'mohammad hmead',NULL,'2020-05-08 18:51:52','2020-05-08 18:50:42','2020-05-08 18:51:52'),(20,'mohammed company','6416512343','45662','amman',1,'079707145641','$2y$10$ej7QxAiaJNStHwy9JN9UheC5it9c0UeSYSswUtqG0BvVDlhrY1d3.','1234567','khalid7','khalid.aldaboubi73@gmail.com',NULL,'mohammed',4,NULL,'2020-05-08 19:02:44','2020-05-08 19:02:44'),(21,'ww','w','w','w',11,'ww','$2y$10$C4XfS72eVRzGGFdTgrGrh.Z/hmPPdzZYcVkUnWImV0BHceZVinjQG','12345678','hmead','ww@gmail.com',NULL,'w111111111111',NULL,NULL,'2020-05-26 14:54:47','2020-05-26 14:54:47'),(22,'asdfghjkl','111111','111111','ءءءءء',9,'22222','$2y$10$R6RhVv7PZO4LTjYa6qniI.5YaCclM3uoAjTihhr94lfHeWTI3CNu6','111111111','1111','os@os.com',NULL,'osamaas',NULL,NULL,'2020-06-03 09:46:07','2020-06-03 09:46:07');
/*!40000 ALTER TABLE `suppliers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `plain_password` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `address` text,
  `country_id` int(11) DEFAULT NULL,
  `city_id` int(11) DEFAULT NULL,
  `street` varchar(255) DEFAULT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `phone` (`phone`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (11,'khalid aldaboubi','0797071441','$2y$10$OH0c34s86Sa92QqybUuMH.hcKaqz85EO9ALLYuozQ2jm3qWihDFPO','1234567','khalid','khalid@gmail.com',NULL,'sahab',2,1,'sahat st','2020-05-04 16:37:25','2020-05-04 16:34:28','2020-05-04 16:37:25'),(13,'hmead','0797071440','$2y$10$T4y//PFqBbFR1jhT5uOVquT1C2PDJ0L3N0r4JfUkSbMQjSFOaSs6G','1234567','hmead','mohammadnhmead@gmail.com',NULL,NULL,NULL,NULL,NULL,'2020-05-05 17:42:40','2020-05-05 17:35:43','2020-05-05 17:42:40');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-06-05 19:35:50
