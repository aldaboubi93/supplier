<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Supplier extends Authenticatable implements JWTSubject, MustVerifyEmail
{
    use Notifiable;

    protected $guard = "suppliers";

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function sendEmailVerificationNotification()
    {
        $this->notify(new Notifications\UserVerificationEmail('suppliers'));
    }


    public function getJWTCustomClaims()
    {
        return [];
    }

    public function attachments()
    {
        return $this->hasMany('App\SupplierAttachment', 'supplier_id');
    }

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
