<?php

namespace App\Exceptions;

use App\Traits\RestExceptionHandlerTrait;
use App\Traits\RestTrait;
use BadMethodCallException;
use Exception;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Validation\ValidationException;

class Handler extends ExceptionHandler
{
    use RestTrait;
    use RestExceptionHandlerTrait;

    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(\Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, \Throwable $exception)
    {
//        $request->headers->set('Accept', 'application/json');
//        $request->header('Authorization',"Bearer ".$request->bearerToken());
//        if($this->isApiCall($request)) {
//            $retval = $this->handleApiException($request, $exception);
//        } else {
//            $retval = parent::render($request, $exception);
//        }

//        return $retval;
        return parent::render($request, $exception);
    }


    private function handleApiException($request, Exception $exception)
    {
        $request->headers->set('Accept', 'application/json');
        $request->headers->set('Access-Control-Allow-Headers',  'Content-Type, X-Auth-Token, Authorization, Origin');

        $request->header('Authorization',"Bearer ".$request->bearerToken());

        $exception = $this->prepareException($exception);

        if ($exception instanceof HttpResponseException) {
            $exception = $exception->getResponse();
        }

        if ($exception instanceof AuthenticationException) {
            $exception = $this->unauthenticated($request, $exception);
        }

        if ($exception instanceof \BadMethodCallException) {
            return response()->view('errors/ControllerNotFound');
        }

        if ($exception instanceof ValidationException) {
            $exception = $this->convertValidationExceptionToResponse($exception, $request);
        }

        return $this->customApiResponse($exception);
    }


    private function customApiResponse($exception)
    {
//        if (method_exists($exception, 'getStatusCode')) {
            $statusCode = $exception->getStatusCode();
//        } else {
//            $statusCode = 500;
//        }

        $response = [];

        switch ($statusCode) {
            case 400:
                $response['message'] = 'dsfsdfsdf';
                break;
            case 401:
                $response['message'] = 'Unauthorized';
                break;
            case 403:
                $response['message'] = 'Forbidden';
                break;
            case 404:
                $response['message'] = 'Not Found';
                break;
            case 405:
                $response['message'] = 'Method Not Allowed';
                break;
            case 422:
                $response['message'] = $exception->original['message'];
                $response['errors'] = $exception->original['errors'];
                break;
            case 429:
                $response['message'] = 'Too Many Requests';
                break;
            default:
                $response['message'] = ($statusCode == 500) ? $exception->getMessage() : $exception->getMessage();
                break;
        }

//        if (config('app.debug')) {
//            $response['trace'] = $exception->getTrace();
//            $response['code'] = $exception->getCode();
//        }

        $response['status'] = $statusCode;

        return response()->json($response, $statusCode);
    }
}
