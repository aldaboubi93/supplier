<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserMessageResource extends JsonResource
{
    public function toArray($request)
    {
        if ($this->sender == 'user'){
            return [
                'id' => $this->id,
                'body' => $this->body,
                'sender' => [
                    'id' => $this->user->id,
                    'name' => $this->user->name
                ],
                'receiver' => [
                    'id' => $this->supplier->id,
                    'name' => $this->supplier->company_name
                ],
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ];
        } else if ($this->sender == 'supplier'){
            return [
                'id' => $this->id,
                'body' => $this->body,
                'sender' => [
                    'id' => $this->supplier->id,
                    'name' => $this->supplier->company_name
                ],
                'receiver' => [
                    'id' => $this->user->id,
                    'name' => $this->user->name
                ],
                'created_at' => $this->created_at,
                'updated_at' => $this->updated_at,
            ];
        }

    }
}
