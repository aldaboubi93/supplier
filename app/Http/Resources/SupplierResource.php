<?php

namespace App\Http\Resources;

use App\SupplierAttachment;
use Illuminate\Http\Resources\Json\JsonResource;

class SupplierResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = [
            'id' => $this->id,
            'company_name' => $this->company_name,
            'registration_number' => $this->registration_number,
            'land_line' => $this->land_line,
            'location' => $this->location,
            'category' => $this->category,
            'phone' => $this->phone,
            'email' => $this->email,
            'username' => $this->username,
            'full_name' => $this->full_name,
            'rate' => $this->rate,
            'attachments' => SupplierAttachmentResource::collection($this->attachments),
            'verified' => $this->email_verified_at == null ? false : true,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        return $data;
    }
}
