<?php

namespace App\Http\Resources;

use App\SubStore;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $sub_store_ids = explode(',',$this->sub_store_ids);
        $sub_stores = SubStore::select('id','ar_name','en_name')->whereIn('id',$sub_store_ids)->get();
        $data = [
            'id' => $this->id,
            'ar_title' => $this->ar_title,
            'en_title' => $this->en_title,
            'ar_description' => $this->ar_description,
            'en_description' => $this->en_description,
            'sub_stores' => $sub_stores,
            'category' => $this->category != null ? [
                'id' => $this->category->id,
                'ar_name' => $this->category->ar_name,
                'en_name' => $this->category->en_name
                ] : null,
            'sub_category' => $this->sub_category != null ? [
                'id' => $this->sub_category->id,
                'ar_name' => $this->sub_category->ar_name,
                'en_name' => $this->sub_category->en_name
            ] : null,
            'supplier' => $this->supplier != null ? new SupplierResource($this->supplier) : null,
            'phone' => $this->phone,
            'location' => $this->location,
            'barcode' => $this->barcode,
            'old_price' => $this->old_price,
            'sale_price' => $this->sale_price,
            'end_date' => $this->end_date,
            'qty' => $this->qty,
            'images' => ProductPhotoResource::collection($this->images),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];

        return $data;
    }
}
