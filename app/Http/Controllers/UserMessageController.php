<?php

namespace App\Http\Controllers;

use App\Http\Resources\SupplierMessageResource;
use App\Http\Resources\UserMessageResource;
use App\Message;
use Illuminate\Http\Request;

class UserMessageController extends BaseController
{
    public function __construct()
    {
        $this->middleware('auth:users');
    }

    public function index()
    {
        $messages = Message::where('user_id',auth('users')->user()->id)->orderByDesc('created_at')->get();
        if ($messages->count() > 0){
            return $this->sendResponse(UserMessageResource::collection($messages),'');
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'body' => 'regex:/(^[A-Za-z0-9 ?!]+$)+/'
        ]);

        $newMessage = new Message();
        $newMessage->user_id = auth('users')->user()->id;
        $newMessage->supplier_id = $request->receiver_id;
        $newMessage->body = $request->body;
        $newMessage->sender = 'user';
        if ($newMessage->save()) {
            return $this->sendResponse('send message success', 'success');
        }
        return $this->sendError('cannot send this message');
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'body' => 'regex:/(^[A-Za-z0-9 ?!]+$)+/'
        ]);

        $message = Message::find($id);
        $message->body = $request->body ?? $message->body;
        if ($message->save()) {
            return $this->sendResponse(new UserMessageResource($message), 'updated');
        }
        return $this->sendError('cannot update this message');
    }

    public function destroy($id)
    {
        $message = Message::find($id);
        if ($message) {
            if ($message->delete()) {
                return $this->sendResponse("", "Deleted");
            }
        }
        return $this->sendError('message not found', 'message not found');
    }
}
