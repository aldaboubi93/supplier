<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Resources\CityResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\SupplierResource;
use App\Product;
use App\ProductPhoto;
use App\Supplier;
use App\SupplierAttachment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ProductController extends BaseController
{
    use ApiResponseTrait;

    public function __construct()
    {
        $this->middleware('auth:suppliers')->except(['index', 'getBySupplierId', 'getByCategoryId',
            'getByEndDate', 'getBySubCategoryId', 'expiredProducts', 'show','search','getByPrice']);
    }

    public function index()
    {
        $products = Product::orderByDesc('end_date')->whereDate('end_date', '>=', Carbon::now()->toDateTimeString())->get();
        if ($products) {
            return $this->sendResponse(ProductResource::collection($products), 'Success');
        }
        return $this->sendError('', 'products not found');
    }

    public function expiredProducts()
    {
        $products = Product::orderByDesc('end_date')->whereDate('end_date', '<', Carbon::now()->toDateTimeString())->get();
        if ($products) {
            return $this->sendResponse(ProductResource::collection($products), 'Success');
        }
        return $this->sendError('', 'products not found');
    }

//    public function getByLatLng(Request $request)
//    {
//        $lat = $request->lat;
//        $lng = $request->lng;
//        $products = Product::whereHas('sub_stores', function ($query) use ($lat,$lng) {
//            $query->where('lat', $lat)->where('lng',$lng);
//        })->orderByDesc('end_date')->get();
//        if ($products->count() > 0) {
//            return $this->sendResponse(ProductResource::collection($products), 'Success');
//        }
//        return $this->sendError('products not found', 'products not found');
//    }


    public function getBySupplierId()
    {
        $products = Product::where('supplier_id', auth('suppliers')->user()->id)->orderByDesc('end_date')->get();
        if ($products->count() > 0) {
            return $this->sendResponse(ProductResource::collection($products), 'Success');
        }
        return $this->sendError('', 'products not found');
    }

    public function search(Request $request)
    {
        $products = Product::where('ar_title','like',"%$request->key%")
            ->orWhere('en_title','like',"%$request->key%")
            ->orWhere('ar_description','like',"%$request->key%")
            ->orWhere('ar_description','like',"%$request->key%")
            ->orWhere('en_description','like',"%$request->key%")
            ->orWhere('location','like',"%$request->key%")
            ->orderByDesc('end_date')
            ->get();

        if ($products->count() > 0) {
            return $this->sendResponse(ProductResource::collection($products), 'Success');
        }
        return $this->sendError('', 'products not found');
    }

    public function getByPrice(Request $request)
    {
        $products = Product::where('sale_price','>=',$request->price_from)
            ->where('sale_price','<=',$request->price_to)
            ->orderByDesc('end_date')
            ->get();

        if ($products->count() > 0) {
            return $this->sendResponse(ProductResource::collection($products), 'Success');
        }
        return $this->sendError('', 'products not found');
    }

    public function getByCategoryId($categoryId)
    {
        $products = Product::where('category_id', $categoryId)->orderByDesc('end_date')->get();
        if ($products->count() > 0) {
            return $this->sendResponse(ProductResource::collection($products), 'Success');
        }
        return $this->sendError('products not found', 'products not found');
    }

    public function getByEndDate($enddate)
    {
        $products = Product::where('end_date', $enddate)->orderByDesc('end_date')->get();
        if ($products->count() > 0) {
            return $this->sendResponse(ProductResource::collection($products), 'Success');
        }
        return $this->sendError('products not found', 'products not found');
    }

    public function getBySubCategoryId($subCategoryId)
    {
        $products = Product::whereHas('sub_category', function ($query) use ($subCategoryId) {
            $query->where('id', $subCategoryId);
        })->orderByDesc('end_date')->get();
        if ($products->count() > 0) {
            return $this->sendResponse(ProductResource::collection($products), 'Success');
        }
        return $this->sendError('products not found', 'products not found');
    }

    public function store(Request $request)
    {

//        return response()->json($_FILES);
//        return Input::file('images');
        $request->validate([
            'ar_title' => 'required',
            'en_title' => 'required',
            'ar_description' => 'required',
            'en_description' => 'required',
            'sub_store_ids' => 'required',
            'category_id' => 'required',
            'old_price' => 'required',
            'sale_price' => 'required',
            'end_date' => 'required',
            'phone' => 'required',
            'location' => 'required',
            'sub_category_id' => 'required',
            'barcode' => 'required',
            'qty' => 'required'
        ]);

        $newProduct = new Product();
        $newProduct->ar_title = $request->ar_title;
        $newProduct->en_title = $request->en_title;
        $newProduct->ar_description = $request->ar_description;
        $newProduct->en_description = $request->en_description;
        $newProduct->sub_store_ids = collect($request->sub_store_ids)->implode(',');
        $newProduct->category_id = $request->category_id;
        $newProduct->old_price = $request->old_price;
        $newProduct->sale_price = $request->sale_price;
        $newProduct->end_date = $request->end_date;
        $newProduct->phone = $request->phone;
        $newProduct->location = $request->location;
        $newProduct->sub_category_id = $request->sub_category_id;
        $newProduct->barcode = $request->barcode;
        $newProduct->qty = $request->qty;
        $newProduct->supplier_id = auth('suppliers')->user()->id;

        if ($newProduct->save()) {
            if ($request->file('images')) {
                $this->uploadPhotos($newProduct, $request->file('images'), 'products');
            }
            return $this->sendResponse(new ProductResource($newProduct), 'Successfully Added');
        }
        return $this->sendError('bad request', 'bad request', 400);
    }



    public function edit(Request $request, $id)
    {
//        return Input::file('images');
        $request->validate([
            'images[]' => 'image|mimes:jpeg,png,jpg|max:2048',
        ]);

        $product = Product::find($id);
        if ($product) {
            $product->ar_title = $request->ar_title ?? $product->ar_title;
            $product->en_title = $request->en_title ?? $product->en_title;
            $product->ar_description = $request->ar_description ?? $product->ar_description;
            $product->en_description = $request->en_description ?? $product->en_description;
            $product->sub_store_ids = collect($request->sub_store_ids)->implode(',') ?? $product->sub_store_ids;
            $product->category_id = $request->category_id ?? $product->category_id;
            $product->old_price = $request->old_price ?? $product->old_price;
            $product->sale_price = $request->sale_price ?? $product->sale_price;
            $product->end_date = $request->end_date ?? $product->end_date;
            $product->phone = $request->phone ?? $product->phone;
            $product->location = $request->location ?? $product->location;
            $product->sub_category_id = $request->sub_category_id ?? $product->sub_category_id;
            $product->barcode = $request->barcode ?? $product->barcode;
            $product->qty = $request->qty ?? $product->qty;
            $product->supplier_id = auth('suppliers')->user()->id ?? $product->ar_title;

            if ($product->save()) {
                if ($request->hasFile('images')) {
                    $this->uploadPhotos($product, $request->file('images'), 'products');
                }
                return $this->sendResponse(new ProductResource($product), 'Successfully Added');
            }
            return $this->sendError('cannot update', 'cannot update');
        }
        return $this->sendError('product not found', 'product not found');
    }

    public function uploadPhotos($product, $images, $folder)
    {
        foreach ($images as $image) {
            $fileName = Carbon::now()->format('YmdHs') . Str::random(4) . "." . $image->extension();
            $image->move(public_path('/images/') . $folder . '/' . $product->id . '/', $fileName);

            $newPhoto = new ProductPhoto();
            $newPhoto->photo_name = $fileName;
            $newPhoto->product_id = $product->id;
            $newPhoto->save();
        }
    }

    public function removePhotos($productPhotos, $folder)
    {
        foreach ($productPhotos as $photo) {
            $oldPhoto = public_path('/images/') . $folder . '/' . $photo->product_id . '/' . $photo->photo_name;
            if (file_exists($oldPhoto)) {
                if (unlink($oldPhoto)) {
                    ProductPhoto::find($photo->id)->delete();
                }
            }
        }
    }


    public function show($id)
    {
        $product = Product::find($id);
        if ($product) {
            return $this->sendResponse(new ProductResource($product), "");
        }
        return $this->sendError('', 'product not found', 200);
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        if ($product) {
            $productPhotos = ProductPhoto::where('product_id', $id)->get();
            $this->removePhotos($productPhotos, 'products');
            if ($product->delete()) {
                return $this->sendResponse("", "Deleted");
            }
        }
        return $this->sendError("", "this product not found");
    }

    public function removeImage($imageId)
    {
        $image = ProductPhoto::find($imageId);
        if ($image) {
            $oldAttachment = public_path('/images/products/') . $image->product_id . '/' . $image->photo_name;
            if (file_exists($oldAttachment)) {
                if (unlink($oldAttachment)) {
                    if (ProductPhoto::find($image->id)->delete()) {
                        return $this->sendResponse("", "Deleted");
                    }
                }
            }
        }

        return $this->sendError("this image not found", "not found");
    }
}
