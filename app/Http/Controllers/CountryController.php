<?php

namespace App\Http\Controllers;

use App\Category;
use App\Country;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\CountryResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\SupplierResource;
use App\Product;
use App\Store;
use App\Supplier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\Input;

class CountryController extends BaseController
{
    use ApiResponseTrait;

    public function __construct()
    {
        $this->middleware('auth:admins')->except(['index','show']);
    }

    public function index()
    {
        $countries = Country::orderByDesc('created_at')->get();
        if ($countries) {
            return $this->sendResponse(CountryResource::collection($countries), 'Success');
        }
        return $this->sendError('countries not found', 'countries not found');
    }

    public function store(Request $request)
    {
        $request->validate([
            'ar_name' => 'required',
            'en_name' => 'required',
            'ar_photo' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'en_photo' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $newCountry = new Country();
        $newCountry->ar_name = $request->ar_name;
        $newCountry->en_name = $request->en_name;
        $newCountry->ar_photo = $this->uploadPhoto($request->file('ar_photo'),'countries') ?? '';
        $newCountry->en_photo = $this->uploadPhoto($request->file('en_photo'),'countries') ?? '';
        if ($newCountry->save()) {
            return $this->sendResponse(new CountryResource($newCountry), 'Successfully Added');
        }
        return $this->sendError('bad request', 'bad request',400);
    }

    public function uploadPhoto($image,$folder)
    {
        $fileName = Carbon::now()->format('YmdHs') . Str::random(4) . "." . $image->extension();
        $image->move(public_path('/images/').$folder, $fileName);
        return $fileName;
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'ar_photo' => 'image|mimes:jpeg,png,jpg|max:2048',
            'en_photo' => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);
        $country = Country::find($id);
        if ($country) {
            if ($request->hasFile('ar_photo')) {
                $oldArImage = public_path('/images/countries/') . $country->ar_photo;
                if (Storage::exists($oldArImage)) {
                    unlink($oldArImage);
                }
            }
            if ($request->hasFile('en_photo')) {
                $oldEnImage = public_path('/images/countries/') . $country->en_photo;
                if (Storage::exists($oldEnImage)) {
                    unlink($oldEnImage);
                }
            }

            $country->ar_name = $request->ar_name ?? $country->ar_name;
            $country->en_name = $request->en_name ?? $country->en_name;
            $country->ar_photo = $this->uploadPhoto($request->file('ar_photo'),'countries') ?? $country->ar_photo;
            $country->en_photo = $this->uploadPhoto($request->file('en_photo'),'countries') ?? $country->ar_photo;
            if ($country->save()) {
                return $this->sendResponse(new CountryResource($country), "Updated");
            }
            return $this->sendError('cannot update', 'cannot update');
        }
        return $this->sendError('country not found', 'country not found');
    }

    public function show($id)
    {
        $country = Country::find($id);
        if ($country) {
            return $this->sendResponse(new CountryResource($country), "");
        }
        return $this->sendError('','country not found',200);
    }

    public function destroy($id)
    {
        $country = Country::find($id);
        if ($country) {
            $oldArImage = public_path('/images/countries/') . $country->ar_photo;
            $oldEnImage = public_path('/images/countries/') . $country->en_photo;
            if (Storage::exists($oldArImage)) {
                unlink($oldArImage);
            }
            if (Storage::exists($oldEnImage)) {
                unlink($oldEnImage);
            }
            if ($country->delete()) {
                return $this->sendResponse("", "Deleted");
            }
        }
        return $this->sendError('country not found', 'country not found');
    }
}
