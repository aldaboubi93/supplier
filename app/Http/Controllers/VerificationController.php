<?php

namespace App\Http\Controllers;

use App\Supplier;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Routing\Route;

class VerificationController extends Controller
{
    /*
     |--------------------------------------------------------------------------
     | Email Verification Controller
     |--------------------------------------------------------------------------
     |
     | This controller is responsible for handling email verification for any
     | user that recently registered with the application. Emails may also
     | be re-sent if the user didn't receive the original email message.
     |
     */

    use VerifiesEmails;

    public function verify(Request $request)
    {
        $userId = $request->route('id');
        $type = $request->type;
        if ($type == 'users'){
            $user = User::findOrFail($userId);
        } else if ($type == 'suppliers'){
            $user = Supplier::findOrFail($userId);
        }

        $date = Carbon::now()->toDateTimeString();

        $user->email_verified_at = $date;
        $user->save();

        return response(['message' => 'Successfully verified']);
    }


}
