<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AdminUserController extends BaseController
{
    use ApiResponseTrait;

    public function __construct()
    {
        $this->middleware('auth:admins');
    }

    public function index()
    {
        $users = User::orderByDesc('created_at')->get();
        if ($users){
            return $this->sendResponse(UserResource::collection($users),'Success');
        }
        return $this->sendError('users not found', 'users not found');
    }



    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required|unique:users',
            'email' => 'required|unique:users',
            'username' => 'required|unique:users',
            'password' => 'required|min:6',
        ]);

        $newUser = new User();
        $newUser->name = $request->name;
        $newUser->phone = $request->phone;
        $newUser->email = $request->email;
        $newUser->username = $request->username;
        $newUser->plain_password = $request->password;
        $newUser->password = bcrypt($request->password);
        $newUser->address = $request->address;
        $newUser->country_id = $request->country_id;
        $newUser->city_id = $request->city_id;
        $newUser->street = $request->street;
        if ($newUser->save()){
            return $this->sendResponse(new UserResource($newUser),'Successfully Added');
        }
        return $this->sendError('bad request', 'bad request',400);
    }

    public function show($id)
    {
        $user = User::find($id);
        if ($user){
            return $this->sendResponse(new UserResource($user),"");
        }
        return $this->sendError('user not found', 'user not found');
    }


    public function update(Request $request,$id)
    {
        $request->validate([
            'phone' => 'unique:users',
            'email' => 'unique:users',
            'username' => 'unique:users',
            'password' => 'min:6',
        ]);

        $user = User::find($id);
        if ($user){
            $user->name = $request->name ?? $user->name;
            $user->phone = $request->phone ?? $user->phone;
            if ($request->password){
                $user->password = bcrypt($request->password);
            }
            $user->username = $request->username ?? $user->username;
            $user->email = $request->email ?? $user->email;
            $user->address = $request->address ?? $user->address;
            $user->country_id = $request->country_id ?? $user->country_id;
            $user->city_id = $request->city_id ?? $user->city_id;
            $user->street = $request->street ?? $user->street;
            $user->email_verified_at = $request->verify = true ? Carbon::now()->toDateTimeString() : null;
            if ($user->save()){
                return $this->sendResponse(new UserResource($user),"Updated");
            }
        }
        return $this->sendError('cannot update', 'cannot update');
    }


    public function destroy($id)
    {
        $user = User::find($id);
        if ($user){
            if ($user->delete()){
                return $this->sendResponse("","Deleted");
            }
        }
        return $this->sendError('user not found', 'user not found');
    }
}
