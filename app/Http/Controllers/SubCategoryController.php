<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Resources\SubCategoryResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\SubStoreResource;
use App\Http\Resources\SupplierResource;
use App\Product;
use App\Store;
use App\SubCategory;
use App\SubStore;
use App\Supplier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\Input;

class SubCategoryController extends BaseController
{
    use ApiResponseTrait;

    public function __construct()
    {
        $this->middleware('auth:admins')->except(['index','getByCategoryId','show']);
    }

    public function index()
    {
        $subCategories = SubCategory::orderByDesc('created_at')->get();
        if ($subCategories){
            return $this->sendResponse(SubCategoryResource::collection($subCategories),'Success');
        }
        return $this->sendError('sub categories not found', 'sub categories not found');
    }

    public function getByCategoryId($categoryId)
    {
        $subCategories = SubCategory::where('category_id',$categoryId)->orderByDesc('created_at')->get();
        if ($subCategories->count() > 0) {
            return $this->sendResponse(SubCategoryResource::collection($subCategories), 'Success');
        }
        return $this->sendError('sub categories not found', 'sub categories not found');
    }

    public function store(Request $request)
    {
        $request->validate([
            'ar_name' => 'required',
            'en_name' => 'required',
            'icon_photo' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'category_id' => 'required',
        ]);

        $fileName = Carbon::now()->format('YmdHs').Str::random(4).".".$request->file('icon_photo')->extension();
        $request->file('icon_photo')->move(public_path('/images/sub_categories/'),$fileName);

        $newSubCategory = new SubCategory();
        $newSubCategory->ar_name = $request->ar_name;
        $newSubCategory->en_name = $request->en_name;
        $newSubCategory->icon_photo = $fileName;
        $newSubCategory->category_id = $request->category_id;
        if ($newSubCategory->save()){
            return $this->sendResponse(new SubCategoryResource($newSubCategory),'Successfully Added');
        }
        return $this->sendError('bad request', 'bad request',400);
    }

    public function update(Request $request,$id)
    {
        $request->validate([
            'icon_photo' => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);
        $subCategory = SubCategory::find($id);
        if ($subCategory){
            if ($request->hasFile('icon_photo')){
                $oldImage = public_path('/images/sub_categories/').$subCategory->icon_photo;
                unlink($oldImage);
                $fileName = Carbon::now()->format('YmdHs').Str::random(4).".".$request->file('icon_photo')->extension();
                $request->file('icon_photo')->move(public_path('/images/sub_categories/'),$fileName);
            }
            $subCategory->ar_name = $request->ar_name ?? $subCategory->ar_name;
            $subCategory->en_name = $request->en_name ?? $subCategory->en_name;
            $subCategory->icon_photo = $fileName ?? $subCategory->icon_photo;
            $subCategory->category_id = $request->category_id ?? $subCategory->category_id;
            if ($subCategory->save()){
                return $this->sendResponse(new SubCategoryResource($subCategory),"Updated");
            }
            return $this->sendError('cannot update', 'cannot update');
        }
        return $this->sendError('sub category not found', 'sub category not found');
    }

    public function show($id)
    {
        $subCategory = SubCategory::find($id);
        if ($subCategory){
            return $this->sendResponse(new SubCategoryResource($subCategory),"");
        }
        return $this->sendError('sub category not found', 'sub category not found');
    }

    public function destroy($id)
    {
        $subCategory = SubCategory::find($id);
        if ($subCategory){
            $oldImage = public_path('/images/sub_categories/').$subCategory->icon_photo;
            if (Storage::exists($oldImage)){
                unlink($oldImage);
            }
            if ($subCategory->delete()){
                return $this->sendResponse("","Deleted");
            }
        }
        return $this->sendError('sub category not found', 'sub category not found');
    }
}
