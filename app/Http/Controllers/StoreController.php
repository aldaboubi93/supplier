<?php

namespace App\Http\Controllers;

use App\Http\Resources\ProductResource;
use App\Product;
use App\Store;
use App\Http\Resources\StoreResource;
use Illuminate\Http\Request;

class StoreController extends BaseController
{
    use ApiResponseTrait;

    public function __construct()
    {
        $this->middleware('auth:suppliers');
    }

    public function index()
    {
        $stores = Store::orderByDesc('created_at')->get();
        if ($stores) {
            return $this->sendResponse(StoreResource::collection($stores), 'Success');
        }
        return $this->sendError('stores not found', 'stores not found');
    }

    public function getBySupplierId()
    {
        $stores = Store::where('supplier_id',auth('suppliers')->user()->id)->orderByDesc('created_at')->get();
        if ($stores->count() > 0) {
            return $this->sendResponse(StoreResource::collection($stores), 'Success');
        }
        return $this->sendError('stores not found', 'stores not found');
    }

    public function store(Request $request)
    {
        $request->validate([
            'ar_name' => 'required',
            'en_name' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ]);

        $newStore = new Store();
        $newStore->ar_name = $request->ar_name;
        $newStore->en_name = $request->en_name;
        $newStore->lat = $request->lat;
        $newStore->lng = $request->lng;
        $newStore->supplier_id = auth('suppliers')->user()->id;
        if ($newStore->save()) {
            return $this->sendResponse(new StoreResource($newStore), 'Successfully Added');
        }
        return $this->sendError('bad request', 'bad request',400);
    }

    public function update(Request $request, $id)
    {

        $store = Store::find($id);
        if ($store) {
            $store->ar_name = $request->ar_name ?? $store->ar_name;
            $store->en_name = $request->en_name ?? $store->en_name;
            $store->lat = $request->lat ?? $store->lat;
            $store->lng = $request->lng ?? $store->lng;
            $store->supplier_id = $request->supplier_id ?? $store->supplier_id;
            if ($store->save()) {
                return $this->sendResponse(new StoreResource($store), "Updated");
            }
            return $this->sendError('cannot update', 'cannot update');
        }
        return $this->sendError('store not found', 'store not found');
    }

    public function show($id)
    {
        $store = Store::find($id);
        if ($store) {
            return $this->sendResponse(new StoreResource($store), "");
        }
        return $this->sendError('','Store not found',200);
    }

    public function destroy($id)
    {
        $store = Store::find($id);
        if ($store) {
            if ($store->delete()) {
                return $this->sendResponse("", "Deleted");
            }
        }
        return $this->sendError('','Store not found',200);
    }
}
