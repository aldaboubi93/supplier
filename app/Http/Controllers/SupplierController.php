<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Http\Resources\AdminResource;
use App\Http\Resources\SupplierResource;
use App\Http\Resources\UserResource;
use App\ProductPhoto;
use App\Supplier;
use App\SupplierAttachment;
use App\User;
use Carbon\Carbon;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class SupplierController extends BaseController
{

    use ApiResponseTrait, VerifiesEmails;

    public function __construct()
    {
        $this->middleware('auth:suppliers', ['except' => ['login', 'register', 'verify']]);
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    public function register(Request $request)
    {
        $request->validate([
            'company_name' => 'required',
            'registration_number' => 'required|unique:suppliers',
            'land_line' => 'required',
            'location' => 'required',
            'category_id' => 'required',
            'username' => 'required|unique:suppliers',
            'email' => 'required|unique:suppliers',
            'phone' => 'required|unique:suppliers',
            'password' => 'required|min:6',
            'full_name' => 'required|min:6',
            'files.*' => 'max:50000|mimes:xlsx,doc,csv,docx,ppt,pptx,ods,odt,odp,txt,jpeg,png,jpg'
        ]);

        $newSupplier = new Supplier();
        $newSupplier->company_name = $request->company_name;
        $newSupplier->registration_number = $request->registration_number;
        $newSupplier->land_line = $request->land_line;
        $newSupplier->location = $request->location;
        $newSupplier->category_id = $request->category_id;
        $newSupplier->phone = $request->phone;
        $newSupplier->email = $request->email;
        $newSupplier->username = $request->username;
        $newSupplier->plain_password = $request->password;
        $newSupplier->password = bcrypt($request->password);
        $newSupplier->full_name = $request->full_name;
        $newSupplier->rate = $request->rate;
        if ($newSupplier->save()) {
            $newSupplier->sendEmailVerificationNotification();
            if ($request->hasFile('files')) {
                $this->uploadAttachments($newSupplier, $request->file('files'), 'suppliers');
            }
            return $this->sendResponse(new SupplierResource($newSupplier), 'Successfully Added');
        }
        return $this->sendError('bad request', 'bad request', 400);
    }


    public function uploadAttachments($supplier, $files, $folder)
    {
        foreach ($files as $file) {
            $fileName = Carbon::now()->format('YmdHs') . Str::random(4) . "." . $file->extension();
            $file->move(public_path('/attachments/') . $folder . '/' . $supplier->id . '/', $fileName);

            $newFile = new SupplierAttachment();
            $newFile->file_name = $fileName;
            $newFile->supplier_id = $supplier->id;
            $newFile->save();
        }
    }

    public function removeAttachment($attachmentId)
    {
        $attachment = SupplierAttachment::find($attachmentId);
        if ($attachment){
            $oldAttachment = public_path('/attachments/suppliers/') . $attachment->supplier_id . '/' . $attachment->file_name;
            if (file_exists($oldAttachment)) {
                if (unlink($oldAttachment)) {
                    if (SupplierAttachment::find($attachment->id)->delete()) {
                        return $this->sendResponse("","Deleted");
                    }
                }
            }
        }

        return $this->sendError("this attachment not found","not found");
    }

    public function resend(Request $request)
    {
        if ($request->user()->hasVerifiedEmail()) {

            return response(['message' => 'Already verified']);
        }

        $request->user()->sendEmailVerificationNotification();

        if ($request->wantsJson()) {
            return response(['message' => 'Email Sent']);
        }

        return back()->with('resent', true);
    }

    public function login(Request $request)
    {
        $password = $request->password;
        $username = $request->username;
        $verified = true;


        if (!$token = Auth::guard('suppliers')->attempt(array('username' => $username, 'password' => $password))) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        if (auth('suppliers')->user()->email_verified_at == null) {
            return response()->json(['error' => 'Please Verify Your Email'], 401);
        }
        return $this->respondWithTokenUser($token, $verified, \auth('suppliers')->user());
    }

    public function respondWithTokenUser($token, $verified, $supplier)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('suppliers')->factory()->getTTL() * 60,
            'verified' => $verified,
            'user' => [
                'id' => $supplier->id,
                'company_name' => $supplier->company_name,
                'registration_number' => $supplier->registration_number,
                'email' => $supplier->email,
                'full_name' => $supplier->full_name,
                'phone' => $supplier->phone,
                'username' => $supplier->username,
            ]
        ]);
    }

    public function logout()
    {
        auth('suppliers')->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

    public function edit(Request $request)
    {
        $request->validate([
            'registration_number' => 'unique:suppliers',
            'username' => 'unique:suppliers',
            'email' => 'unique:suppliers',
            'phone' => 'unique:suppliers',
            'password' => 'min:6',
            'files.*' => 'max:50000|mimes:xlsx,doc,csv,docx,ppt,pptx,ods,odt,odp,txt,jpeg,png,jpg'
        ]);

        $supplier = Supplier::find(\auth('suppliers')->user()->id);
        if ($supplier) {
            $supplier->company_name = $request->company_name ?? $supplier->company_name;
            $supplier->registration_number = $request->registration_number ?? $supplier->registration_number;
            $supplier->land_line = $request->land_line ?? $supplier->land_line;
            $supplier->location = $request->location ?? $supplier->location;
            $supplier->phone = $request->phone ?? $supplier->phone;
            if ($request->password) {
                $supplier->password = bcrypt($request->password);
            }
            $supplier->username = $request->username ?? $supplier->username;
            $supplier->email = $request->email ?? $supplier->email;
            $supplier->full_name = $request->full_name ?? $supplier->full_name;
            $supplier->rate = $request->rate ?? $supplier->rate;
            if ($supplier->save()) {
                if ($request->hasFile('files')) {
                    $this->uploadAttachments($supplier, $request->file('files'), 'suppliers');
                }
                return $this->sendResponse(new SupplierResource($supplier), "Updated");
            }
        }
    }

    public function show()
    {
        $supplier = Supplier::find(\auth('suppliers')->user()->id);
        if ($supplier) {
            return $this->sendResponse(new SupplierResource($supplier), "");
        }
    }

//    public function destroy()
//    {
//        $supplier = Supplier::find(\auth('suppliers')->user()->id);
//        if ($supplier){
//            if ($supplier->delete()){
//                $attachments = SupplierAttachment::where('supplier_id', \auth('suppliers')->user()->id)->get();
//                $this->removeAttachments($attachments, 'suppliers');
//                return $this->sendResponse("","Deleted");
//            }
//        }
//    }
}
