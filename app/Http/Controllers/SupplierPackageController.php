<?php

namespace App\Http\Controllers;

use App\Category;
use App\Country;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\CountryResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\SupplierPackageResource;
use App\Http\Resources\SupplierResource;
use App\Product;
use App\Store;
use App\Supplier;
use App\SupplierPackage;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\Input;

class SupplierPackageController extends BaseController
{
    use ApiResponseTrait;

    public function __construct()
    {
        $this->middleware('auth:admins');
    }

    public function index()
    {
        $supplierPackages = SupplierPackage::orderByDesc('created_at')->get();
        if ($supplierPackages) {
            return $this->sendResponse(SupplierPackageResource::collection($supplierPackages), 'Success');
        }
        return $this->sendError('countries not found', 'countries not found');
    }

    public function store(Request $request)
    {
        $request->validate([
            'allow_post' => 'required',
            'supplier_id' => 'required',
        ]);

        $newSupplierPackage = new SupplierPackage();
        $newSupplierPackage->allow_post = $request->allow_post;
        $newSupplierPackage->supplier_id = $request->supplier_id;
        if ($newSupplierPackage->save()) {
            return $this->sendResponse(new SupplierPackageResource($newSupplierPackage), 'Successfully Added');
        }
        return $this->sendError('bad request', 'bad request',400);
    }

    public function update(Request $request, $id)
    {

        $supplierPackage = SupplierPackage::find($id);
        if ($supplierPackage) {
            $supplierPackage->allow_post = $request->allow_post ?? $supplierPackage->allow_post;
            $supplierPackage->supplier_id = $request->supplier_id ?? $supplierPackage->supplier_id;
            if ($supplierPackage->save()) {
                return $this->sendResponse(new SupplierPackageResource($supplierPackage), "Updated");
            }
            return $this->sendError('cannot update', 'cannot update');
        }
        return $this->sendError('supplierPackage not found', 'supplierPackage not found');
    }

    public function show($id)
    {
        $supplierPackage = SupplierPackage::find($id);
        if ($supplierPackage) {
            return $this->sendResponse(new SupplierPackageResource($supplierPackage), "");
        }
        return $this->sendError('','supplierPackage not found',200);
    }

    public function destroy($id)
    {
        $supplierPackage = SupplierPackage::find($id);
        if ($supplierPackage) {
            if ($supplierPackage->delete()) {
                return $this->sendResponse("", "Deleted");
            }
        }
        return $this->sendError('supplierPackage not found', 'supplierPackage not found');
    }
}
