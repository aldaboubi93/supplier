<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\ProductResource;
use App\Http\Resources\SupplierResource;
use App\Product;
use App\Store;
use App\Supplier;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Component\Console\Input\Input;

class CategoryController extends BaseController
{
    use ApiResponseTrait;

    public function __construct()
    {
        $this->middleware('auth:admins')->except(['index','show']);
    }

    public function index()
    {
        $categories = Category::orderByDesc('created_at')->get();
        if ($categories){
            return $this->sendResponse(CategoryResource::collection($categories),'Success');
        }
        return $this->sendError('categories not found', 'categories not found');
    }

    public function store(Request $request)
    {
        $request->validate([
            'ar_name' => 'required',
            'en_name' => 'required',
            'icon_photo' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ]);

        $fileName = Carbon::now()->format('YmdHs').Str::random(4).".".$request->file('icon_photo')->extension();
        $request->file('icon_photo')->move(public_path('/images/categories/'),$fileName);

        $newCategory = new Category();
        $newCategory->ar_name = $request->ar_name;
        $newCategory->en_name = $request->en_name;
        $newCategory->icon_photo = $fileName;
        if ($newCategory->save()){
            return $this->sendResponse(new CategoryResource($newCategory),'Successfully Added');
        }
        return $this->sendError('bad request', 'bad request',400);
    }

    public function update(Request $request,$id)
    {
        $request->validate([
            'icon_photo' => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);
        $category = Category::find($id);
        if ($category){
            if ($request->hasFile('icon_photo')){
                $oldImage = public_path('/images/categories/').$category->icon_photo;
                unlink($oldImage);
                $fileName = Carbon::now()->format('YmdHs').Str::random(4).".".$request->file('icon_photo')->extension();
                $request->file('icon_photo')->move(public_path('/images/categories/'),$fileName);
            }
            $category->ar_name = $request->ar_name ?? $category->ar_name;
            $category->en_name = $request->en_name ?? $category->en_name;
            $category->icon_photo = $fileName ?? $category->icon_photo;
            if ($category->save()){
                return $this->sendResponse(new CategoryResource($category),"Updated");
            }
            return $this->sendError('cannot update', 'cannot update');
        }
        return $this->sendError('category not found', 'category not found');
    }

    public function show($id)
    {
        $category = Category::find($id);
        if ($category){
            return $this->sendResponse(new CategoryResource($category),"");
        }
        return $this->sendError('category not found', 'category not found');
    }

    public function destroy($id)
    {
        $category = Category::find($id);
        if ($category){
            $oldImage = public_path('/images/categories/').$category->icon_photo;
            if (Storage::exists($oldImage)){
                unlink($oldImage);
            }
            if ($category->delete()){
                return $this->sendResponse("","Deleted");
            }
        }
        return $this->sendError('category not found', 'category not found');
    }
}
