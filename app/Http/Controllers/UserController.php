<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Favorite;
use App\Http\Resources\AdminResource;
use App\Http\Resources\FavoriteResource;
use App\Http\Resources\UserResource;
use App\Product;
use App\Supplier;
use App\User;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\VerifiesEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends BaseController
{

    use ApiResponseTrait, VerifiesEmails;

    public function __construct()
    {
        $this->middleware('auth:users', ['except' => ['login', 'register', 'verify']]);
        $this->middleware('signed')->only('verify');
        $this->middleware('throttle:6,1')->only('verify', 'resend');
    }

    public function register(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required|unique:users',
            'email' => 'required|unique:users',
            'username' => 'required|unique:users',
            'password' => 'required|min:6',
        ]);

        $newUser = new User();
        $newUser->name = $request->name;
        $newUser->phone = $request->phone;
        $newUser->email = $request->email;
        $newUser->username = $request->username;
        $newUser->plain_password = $request->password;
        $newUser->password = bcrypt($request->password);
        $newUser->address = $request->address;
        $newUser->country_id = $request->country_id;
        $newUser->city_id = $request->city_id;
        $newUser->street = $request->street;
        if ($newUser->save()) {
            $newUser->sendEmailVerificationNotification();
            return $this->sendResponse(new UserResource($newUser), 'Successfully Added');
        }
        return $this->sendError('bad request', 'bad request', 400);
    }

    public function getFavorites()
    {
        $favorites = Favorite::orderByDesc('created_at')->where('user_id', \auth('users')->user()->id)->get();
        if ($favorites) {
            return $this->sendResponse(FavoriteResource::collection($favorites), 'Success');
        }
        return $this->sendError('favorites not found', 'not found');
    }

    public function setFavorite($productId)
    {
        $checkProduct = Product::find($productId);
        if ($checkProduct){
            $favorite = new Favorite();
            $favorite->product_id = $productId;
            $favorite->user_id = \auth('users')->user()->id;
            if ($favorite->save()){
                return $this->sendResponse(new FavoriteResource($favorite),'Success');
            }
            return $this->sendError('cannot insert this product', 'cannot insert this product');
        }
        return $this->sendError('product not found', 'not found');

    }

    public function resend(Request $request)
    {
        if ($request->user()->hasVerifiedEmail()) {

            return response(['message' => 'Already verified']);
        }
        $request->user()->sendEmailVerificationNotification();

        if ($request->wantsJson()) {
            return response(['message' => 'Email Sent']);
        }

        return back()->with('resent', true);
    }

    public function login(Request $request)
    {
        $password = $request->password;
        $username = $request->username;
        $verified = true;


        if (!$token = Auth::guard('users')->attempt(array('username' => $username, 'password' => $password))) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        if (auth('users')->user()->email_verified_at == null){
            return response()->json(['error' => 'Please Verify Your Email'], 401);
        }
        return $this->respondWithTokenUser($token, $verified, \auth('users')->user());
    }

    protected function respondWithTokenUser($token, $verified, $user)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('users')->factory()->getTTL() * 60,
            'verified' => $verified,
            'user' => [
                'id' => $user->id,
                'company_name' => $user->name,
                'email' => $user->email,
                'phone' => $user->phone,
                'username' => $user->username,
            ]
        ]);
    }

    public function logout()
    {
        auth('users')->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

    public function edit(Request $request)
    {

        $request->validate([
            'phone' => 'unique:users',
            'email' => 'unique:users',
            'username' => 'unique:users',
            'password' => 'min:6',
        ]);

        $user = User::find(\auth('users')->user()->id);
        if ($user) {
            $user->name = $request->name ?? $user->name;
            $user->phone = $request->phone ?? $user->phone;
            if ($request->password) {
                $user->password = bcrypt($request->password);
            }
            $user->username = $request->username ?? $user->username;
            $user->email = $request->email ?? $user->email;
            $user->address = $request->address ?? $user->address;
            $user->country_id = $request->country_id ?? $user->country_id;
            $user->city_id = $request->city_id ?? $user->city_id;
            $user->street = $request->street ?? $user->street;
            if ($user->save()) {
                return $this->sendResponse(new UserResource($user), "Updated");
            }
            return $this->sendError('cannot update', 'cannot update');
        }
        return $this->sendError('user not found', 'user not found');
    }


    public function show()
    {
        $user = User::find(\auth('users')->user()->id);
        if ($user) {
            return $this->sendResponse(new UserResource($user), "");
        }
        return $this->sendError('user not found', 'user not found');
    }

    public function destroy()
    {
        $user = User::find(\auth('users')->user()->id);
        if ($user) {
            if ($user->delete()) {
                return $this->sendResponse("", "Deleted");
            }
        }
        return $this->sendError('user not found', 'user not found');
    }
}
