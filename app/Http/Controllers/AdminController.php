<?php

namespace App\Http\Controllers;

use App\Admin;
use App\Employee;
use App\Http\Controllers\ApiResponseTrait;
use App\Http\Resources\AdminResource;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminController extends BaseController
{

    use ApiResponseTrait;

    public function __construct()
    {
        $this->middleware('auth:admins', ['except' => ['login', 'refresh']]);
    }

    protected function index()
    {
        if (\auth('admins')->user()->role != 2){
            return $this->sendError('permission denied', 'permission denied');
        }
        $admins = Admin::where('role',1)->orderByDesc('created_at')->get();
        if ($admins){
            return $this->sendResponse(AdminResource::collection($admins),'Success');
        }
        return $this->sendError('admins not found', 'admins not found');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'phone' => 'required|unique:admins',
            'email' => 'required|unique:admins',
            'username' => 'required|unique:admins',
            'password' => 'required|min:6'
        ]);

        $newAdmin = new Admin();
        $newAdmin->name = $request->name;
        $newAdmin->phone = $request->phone;
        $newAdmin->email = $request->email;
        $newAdmin->username = $request->username;
        $newAdmin->plain_password = $request->password;
        $newAdmin->password = bcrypt($request->password);
        $newAdmin->role = 1;
        if ($newAdmin->save()){
            return $this->sendResponse(new AdminResource($newAdmin),'Successfully Added');
        }
        return $this->sendError('bad request', 'bad request',400);
    }

    public function show()
    {
        $admin = Admin::find(\auth('admins')->user()->id);
        if ($admin){
            return $this->sendResponse(new AdminResource($admin),"");
        }
        return $this->sendError('admin not found', 'admin not found');
    }

    public function edit(Request $request)
    {
        $request->validate([
            'phone' => 'unique:admins',
            'email' => 'unique:admins',
            'username' => 'unique:admins',
            'password' => 'min:6'
        ]);

        $admin = Admin::find(\auth('admins')->user()->id);
        if ($admin){
            $admin->name = $request->name ?? $admin->name;
            $admin->phone = $request->phone ?? $admin->phone;
            if ($request->password){
                $admin->password = bcrypt($request->password);
            }
            $admin->username = $request->username ?? $admin->username;
            $admin->email = $request->email ?? $admin->email;
            $admin->role = $request->role ?? $admin->role;
            if ($admin->save()){
                return $this->sendResponse(new AdminResource($admin),"Updated");
            }
            return $this->sendError('cannot update', 'cannot update');
        }
        return $this->sendError('admin not found', 'admin not found');

    }

    public function editFromSuperAdmin(Request $request,$id)
    {
        $request->validate([
            'phone' => 'unique:admins',
            'email' => 'unique:admins',
            'username' => 'unique:admins',
            'password' => 'min:6'
        ]);

        $admin = Admin::find($id);
        if ($admin){
            $admin->name = $request->name ?? $admin->name;
            $admin->phone = $request->phone ?? $admin->phone;
            $admin->password = bcrypt($request->password) ?? bcrypt($admin->plain_password);
            $admin->username = $request->username ?? $admin->username;
            $admin->email = $request->email ?? $admin->email;
            $admin->role = $request->role ?? $admin->role;
            if ($admin->save()){
                return $this->sendResponse(new AdminResource($admin),"Updated");
            }
            return $this->sendError('cannot update', 'cannot update');
        }
        return $this->sendError('admin not found', 'admin not found');

    }

    public function destroy()
    {
        $admin = Admin::find(\auth('admins')->user()->id);
        if ($admin){
            if ($admin->delete()){
                return $this->sendResponse("","Deleted");
            }
            return $this->sendError('cannot delete', 'cannot delete');
        }
        return $this->sendError('admin not found', 'admin not found');
    }

    public function destroyFromSuperAdmin($id)
    {
        $admin = Admin::find($id);
        if ($admin){
            if ($admin->delete()){
                return $this->sendResponse("","Deleted");
            }
            return $this->sendError('cannot delete', 'cannot delete');
        }
        return $this->sendError('admin not found', 'admin not found');
    }



    public function login(Request $request)
    {
        $password = $request->password;
        $username = $request->username;

        if (!$token = Auth::guard('admins')->attempt(array('username' => $username, 'password' => $password))) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        return $this->respondWithToken($token,'admins');
    }
    public function logout()
    {
        auth('admins')->logout();
        return response()->json(['message' => 'Successfully logged out']);
    }

}
