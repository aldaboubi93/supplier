<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Resources\CityResource;
use App\Http\Resources\SubCategoryResource;
use App\SubCategory;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CityController extends BaseController
{
    use ApiResponseTrait;

    public function __construct()
    {
        $this->middleware('auth:admins')->except(['index','show','getByCountryId']);
    }

    public function index()
    {
        $cities = City::orderByDesc('created_at')->get();
        if ($cities) {
            return $this->sendResponse(CityResource::collection($cities), 'Success');
        }
        return $this->sendError('cities not found', 'cities not found');
    }

    public function getByCountryId($countryId)
    {
        $cities = City::where('country_id',$countryId)->orderByDesc('created_at')->get();
        if ($cities->count() > 0) {
            return $this->sendResponse(CityResource::collection($cities), 'Success');
        }
        return $this->sendError('cities not found', 'cities not found');
    }

    public function store(Request $request)
    {
        $request->validate([
            'ar_name' => 'required',
            'en_name' => 'required',
            'ar_photo' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'en_photo' => 'required|image|mimes:jpeg,png,jpg|max:2048',
            'country_id' => 'required'
        ]);

        $newCity = new City();
        $newCity->ar_name = $request->ar_name;
        $newCity->en_name = $request->en_name;
        $newCity->ar_photo = $this->uploadPhoto($request->file('ar_photo'),'cities') ?? '';
        $newCity->en_photo = $this->uploadPhoto($request->file('en_photo'),'cities') ?? '';
        $newCity->country_id = $request->country_id;
        if ($newCity->save()) {
            return $this->sendResponse(new CityResource($newCity), 'Successfully Added');
        }
        return $this->sendError('bad request', 'bad request',400);
    }

    public function uploadPhoto($image,$folder)
    {
        $fileName = Carbon::now()->format('YmdHs') . Str::random(4) . "." . $image->extension();
        $image->move(public_path('/images/').$folder, $fileName);
        return $fileName;
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'ar_photo' => 'image|mimes:jpeg,png,jpg|max:2048',
            'en_photo' => 'image|mimes:jpeg,png,jpg|max:2048'
        ]);
        $city = City::find($id);
        if ($city) {
            if ($request->hasFile('ar_photo')) {
                $oldArImage = public_path('/images/cities/') . $city->ar_photo;
                if (Storage::exists($oldArImage)) {
                    unlink($oldArImage);
                }
            }
            if ($request->hasFile('en_photo')) {
                $oldEnImage = public_path('/images/cities/') . $city->en_photo;
                if (Storage::exists($oldEnImage)) {
                    unlink($oldEnImage);
                }
            }

            $city->ar_name = $request->ar_name ?? $city->ar_name;
            $city->en_name = $request->en_name ?? $city->en_name;
            $city->ar_photo = $this->uploadPhoto($request->file('ar_photo'),'cities') ?? $city->ar_photo;
            $city->en_photo = $this->uploadPhoto($request->file('en_photo'),'cities') ?? $city->ar_photo;
            $city->country_id = $request->country_id ?? $city->country_id;
            if ($city->save()) {
                return $this->sendResponse(new CityResource($city), "Updated");
            }
            return $this->sendError('cannot update', 'cannot update');
        }
        return $this->sendError('city not found', 'city not found');
    }

    public function show($id)
    {
        $city = City::find($id);
        if ($city) {
            return $this->sendResponse(new CityResource($city), "");
        }
        return $this->sendError('','city not found',200);
    }

    public function destroy($id)
    {
        $city = City::find($id);
        if ($city) {
            $oldArImage = public_path('/images/cities/') . $city->ar_photo;
            $oldEnImage = public_path('/images/cities/') . $city->en_photo;
            if (Storage::exists($oldArImage)) {
                unlink($oldArImage);
            }
            if (Storage::exists($oldEnImage)) {
                unlink($oldEnImage);
            }
            if ($city->delete()) {
                return $this->sendResponse("", "Deleted");
            }
        }
        return $this->sendError('city not found', 'city not found');
    }
}
