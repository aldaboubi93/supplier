<?php

namespace App\Http\Controllers;

use App\Http\Resources\StoreResource;
use App\Store;
use App\Http\Resources\SubStoreResource;
use App\SubStore;
use Illuminate\Http\Request;

class SubStoreController extends BaseController
{
    use ApiResponseTrait;

    public function __construct()
    {
        $this->middleware('auth:suppliers');
    }

    public function index()
    {
        $subStores = SubStore::orderByDesc('created_at')->get();
        if ($subStores) {
            return $this->sendResponse(SubStoreResource::collection($subStores), 'Success');
        }
        return $this->sendError('sub stores not found', 'sub stores not found');
    }

    public function getByStoreId($storeId)
    {
        $subStores = SubStore::where('store_id',$storeId)->orderByDesc('created_at')->get();
        if ($subStores->count() > 0) {
            return $this->sendResponse(SubStoreResource::collection($subStores), 'Success');
        }
        return $this->sendError('sub stores not found', 'sub stores not found');
    }

    public function store(Request $request)
    {
        $request->validate([
            'ar_name' => 'required',
            'en_name' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'store_id' => 'required'
        ]);

        $newSubStore = new SubStore();
        $newSubStore->ar_name = $request->ar_name;
        $newSubStore->en_name = $request->en_name;
        $newSubStore->lat = $request->lat;
        $newSubStore->lng = $request->lng;
        $newSubStore->store_id = $request->store_id;
        if ($newSubStore->save()) {
            return $this->sendResponse(new SubStoreResource($newSubStore), 'Successfully Added');
        }
        return $this->sendError('bad request', 'bad request',400);
    }

    public function update(Request $request, $id)
    {

        $substore = SubStore::find($id);
        if ($substore) {
            $substore->ar_name = $request->ar_name ?? $substore->ar_name;
            $substore->en_name = $request->en_name ?? $substore->en_name;
            $substore->lat = $request->lat ?? $substore->lat;
            $substore->lng = $request->lng ?? $substore->lng;
            $substore->store_id = $request->store_id ?? $substore->store_id;
            if ($substore->save()) {
                return $this->sendResponse(new SubStoreResource($substore), "Updated");
            }
            return $this->sendError('cannot update', 'cannot update');
        }
        return $this->sendError('sub store not found', 'sub store not found');
    }

    public function show($id)
    {
        $substore = SubStore::find($id);
        if ($substore) {
            return $this->sendResponse(new SubStoreResource($substore), "");
        }
        return $this->sendError('','Sub Store not found',200);
    }

    public function destroy($id)
    {
        $substore = SubStore::find($id);
        if ($substore) {
            if ($substore->delete()) {
                return $this->sendResponse("", "Deleted");
            }
        }
        return $this->sendError('','Sub Store not found',200);
    }
}
