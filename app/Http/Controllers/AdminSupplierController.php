<?php

namespace App\Http\Controllers;

use App\Http\Resources\SupplierResource;
use App\Supplier;
use App\SupplierAttachment;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AdminSupplierController extends BaseController
{
    use ApiResponseTrait;

    public function __construct()
    {
        $this->middleware('auth:admins');
    }

    public function index()
    {
        $suppliers = Supplier::orderByDesc('created_at')->get();
        if ($suppliers){
            return $this->sendResponse(SupplierResource::collection($suppliers),'Success');
        }
        return $this->sendError('suppliers not found', 'suppliers not found');
    }

    public function store(Request $request)
    {
        $request->validate([
            'company_name' => 'required',
            'registration_number' => 'required|unique:suppliers',
            'land_line' => 'required',
            'location' => 'required',
            'category_id' => 'required',
            'username' => 'required|unique:suppliers',
            'email' => 'required|unique:suppliers',
            'phone' => 'required|unique:suppliers',
            'password' => 'required|min:6',
            'full_name' => 'required|min:6',
            'files.*'=>'max:50000|mimes:xlsx,doc,csv,docx,ppt,pptx,ods,odt,odp,txt'
        ]);

        $newSupplier = new Supplier();
        $newSupplier->company_name = $request->company_name;
        $newSupplier->registration_number = $request->registration_number;
        $newSupplier->land_line = $request->land_line;
        $newSupplier->location = $request->location;
        $newSupplier->category_id = $request->category_id;
        $newSupplier->phone = $request->phone;
        $newSupplier->email = $request->email;
        $newSupplier->username = $request->username;
        $newSupplier->plain_password = $request->password;
        $newSupplier->password = bcrypt($request->password);
        $newSupplier->full_name = $request->full_name;
        $newSupplier->rate = $request->rate;
        $newSupplier->email_verified_at = Carbon::now()->toDateTimeString();
        if ($newSupplier->save()){
            if ($request->hasFile('files')) {
                $this->uploadAttachments($newSupplier, $request->file('files'), 'suppliers');
            }
            return $this->sendResponse(new SupplierResource($newSupplier),'Successfully Added');
        }
        return $this->sendError('bad request', 'bad request',400);
    }

    public function show($id)
    {
        $supplier = Supplier::find($id);
        if ($supplier){
            return $this->sendResponse(new SupplierResource($supplier),"");
        }
        return $this->sendError('supplier not found', 'supplier not found');
    }

    public function update(Request $request,$id)
    {

        $request->validate([
            'registration_number' => 'unique:suppliers',
            'username' => 'unique:suppliers',
            'email' => 'unique:suppliers',
            'phone' => 'unique:suppliers',
            'password' => 'min:6',
            'files.*'=>'max:50000|mimes:xlsx,doc,csv,docx,ppt,pptx,ods,odt,odp,txt'
        ]);

        $supplier = Supplier::find($id);
        if ($supplier){
            $supplier->company_name = $request->company_name ?? $supplier->company_name;
            $supplier->registration_number = $request->registration_number ?? $supplier->registration_number;
            $supplier->land_line = $request->land_line ?? $supplier->land_line;
            $supplier->location = $request->location ?? $supplier->location;
            $supplier->phone = $request->phone ?? $supplier->phone;
            if ($request->password){
                $supplier->password = bcrypt($request->password);
            }
            $supplier->username = $request->username ?? $supplier->username;
            $supplier->email = $request->email ?? $supplier->email;
            $supplier->full_name = $request->full_name ?? $supplier->full_name;
            $supplier->rate = $request->rate ?? $supplier->rate;
            $supplier->email_verified_at = $request->verify = true ? Carbon::now()->toDateTimeString() : null;
            if ($supplier->save()){
                if ($request->hasFile('files')) {
                    $this->uploadAttachments($supplier, $request->file('files'), 'suppliers');
                }
                return $this->sendResponse(new SupplierResource($supplier),"Updated");
            }
            return $this->sendError('cannot update', 'cannot update');
        }
        return $this->sendError('supplier not found', 'supplier not found');
    }

    public function uploadAttachments($supplier, $files, $folder)
    {
        foreach ($files as $file) {
            $fileName = Carbon::now()->format('YmdHs') . Str::random(4) . "." . $file->extension();
            $file->move(public_path('/attachments/') . $folder . '/' . $supplier->id . '/', $fileName);

            $newFile = new SupplierAttachment();
            $newFile->file_name = $fileName;
            $newFile->supplier_id = $supplier->id;
            $newFile->save();
        }
    }

    public function removeAttachments($attachments, $folder)
    {
        foreach ($attachments as $attachment) {
            $oldAttachment = public_path('/attachments/') . $folder . '/' . $attachment->supplier_id . '/' . $attachment->file_name;
            if (file_exists($oldAttachment)) {
                if (unlink($oldAttachment)) {
                    SupplierAttachment::find($attachment->id)->delete();
                }
            }
        }
    }

    public function destroy($id)
    {
        $supplier = Supplier::find($id);
        if ($supplier){
            if ($supplier->delete()){
                $attachments = SupplierAttachment::where('supplier_id', $id)->get();
                $this->removeAttachments($attachments, 'suppliers');
                return $this->sendResponse("","Deleted");
            }
        }
        return $this->sendError('supplier not found', 'supplier not found');
    }
}
