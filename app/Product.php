<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function category()
    {
        return $this->belongsTo('App\Category', 'category_id');
    }

    public function sub_category()
    {
        return $this->belongsTo('App\SubCategory', 'sub_category_id');
    }
    public function sub_stores()
    {
        return $this->hasMany('App\SubStore', 'sub_store_ids');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Supplier', 'supplier_id');
    }

    public function images()
    {
        return $this->hasMany('App\ProductPhoto', 'product_id');
    }

//    public function scopePreOrder($query)
//    {
//        return $query->where('available_on', '>', new \DateTime())->where('pre_order', true);
//    }
}
